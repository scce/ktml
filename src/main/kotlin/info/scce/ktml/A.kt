package info.scce.ktml

import info.scce.ktml.interfaces.*
import info.scce.ktml.util.AbstractTagBuilder
import info.scce.ktml.util.EmptyClasses
import info.scce.ktml.util.Tag

/**
 * In HTML, the `<a>` tag, commonly known as the anchor tag, is used to create hyperlinks.
 * Hyperlinks are elements on a web page that allow users to navigate to another resource, which could be another web
 * page, an image, a video, a document, or any other type of file accessible via a URL (Uniform Resource Locator).
 *
 * <i>(ChatGPT, February 23, 2024)</i>
 */
class A(
    private val attributes: Attributes,
    private val classes: ClassesInterface,
    private val href: HrefInterface,
    private val children: List<InlineTagInterface>,
) : AbstractTagBuilder<TagInterface>(), AInterface {


    constructor(
        href: HrefInterface,
        vararg children: InlineTagInterface,
    ) : this(Attributes(), EmptyClasses(), href, children.toList())

    constructor(
        attributes: Attributes,
        href: HrefInterface,
        vararg children: InlineTagInterface,
    ) : this(attributes, EmptyClasses(), href, children.toList())

    constructor(
        attributes: Attributes,
        classes: ClassesInterface,
        href: HrefInterface,
        vararg children: InlineTagInterface,
    ) : this(attributes, classes, href, children.toList())

    constructor(
        classes: ClassesInterface,
        href: HrefInterface,
        vararg children: InlineTagInterface,
    ) : this(Attributes(), classes, href, children.toList())

    constructor(
        classes: ClassesInterface,
        href: HrefInterface,
        children: List<InlineTagInterface>,
    ) : this(Attributes(), classes, href, children)

    override fun build(): TagInterface {
        return Tag("a", children, Attributes(classes, href, attributes = attributes))
    }
}