package info.scce.ktml

import info.scce.ktml.interfaces.*
import info.scce.ktml.util.AbstractTagBuilder
import info.scce.ktml.util.EmptyClasses
import info.scce.ktml.util.Tag

/**
 * In HTML, the `<h4>` tag is used to define a level 4 heading.
 * It's part of a set of heading tags ranging from `<h1>` to `<h6>`, with `<h1>` being the highest level of heading
 * and `<h6>` being the lowest.
 *
 * The `<h4>` tag is typically used to represent headings of even lower importance compared to headings defined
 * with `<h1>`, `<h2>`, and `<h3>`. It's often used for subsections or headings within a section.
 *
 * <i>(ChatGPT, February 23, 2024)</i>
 */
class H4(
    // todo this should use AttributesInterface
    private val attributes: Attributes,
    private val classes: AttributeInterface,
    private val children: List<InlineTagInterface>,
) : AbstractTagBuilder<TagInterface>(), H4Interface {

    constructor(
        vararg children: InlineTagInterface,
    ) : this(Attributes(), EmptyClasses(), children.toList())

    constructor(
        attributes: Attributes,
        vararg children: InlineTagInterface,
    ) : this(attributes, EmptyClasses(), children.toList())

    constructor(
        attributes: Attributes,
        classes: ClassesInterface,
        vararg children: InlineTagInterface,
    ) : this(attributes, classes, children.toList())

    constructor(
        classes: ClassesInterface,
        vararg children: InlineTagInterface,
    ) : this(Attributes(), classes, children.toList())

    constructor(
        classes: ClassesInterface,
        children: List<InlineTagInterface>,
    ) : this(Attributes(), classes, children)

    override fun build(): TagInterface {
        return Tag("h4", children, Attributes(classes, attributes = attributes))
    }
}
