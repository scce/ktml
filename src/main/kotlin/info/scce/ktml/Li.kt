package info.scce.ktml

import info.scce.ktml.interfaces.ClassesInterface
import info.scce.ktml.interfaces.LiInterface
import info.scce.ktml.interfaces.TagInterface
import info.scce.ktml.util.AbstractTagBuilder
import info.scce.ktml.util.EmptyClasses
import info.scce.ktml.util.Tag

/**
 * In HTML, the `<li>` tag is used to define a list item within an ordered list (`<ol>`) or an unordered list (`<ul>`).
 * It is a structural element used to represent each individual item in a list.
 *
 * <i>(ChatGPT, February 23, 2024)</i>
 */
class Li(
    private val attributes: Attributes,
    private val classes: ClassesInterface,
    private val children: List<TagInterface>,
) : AbstractTagBuilder<TagInterface>(), LiInterface {
    constructor(
        vararg children: TagInterface,
    ) : this(Attributes(), EmptyClasses(), children.toList())

    constructor(
        attributes: Attributes,
        vararg children: TagInterface,
    ) : this(attributes, EmptyClasses(), children.toList())

    constructor(
        attributes: Attributes,
        classes: ClassesInterface,
        vararg children: TagInterface,
    ) : this(attributes, classes, children.toList())

    constructor(
        classes: ClassesInterface,
        vararg children: TagInterface,
    ) : this(Attributes(), classes, children.toList())

    constructor(
        classes: ClassesInterface,
        children: List<TagInterface>,
    ) : this(Attributes(), classes, children)

    override fun build(): TagInterface {
        return Tag("li", children, Attributes(classes, attributes = attributes))
    }
}
