package info.scce.ktml

import info.scce.ktml.interfaces.MetaInterface
import info.scce.ktml.interfaces.TagInterface
import info.scce.ktml.util.AbstractTagBuilder
import info.scce.ktml.util.SelfClosingTag

/**
 * In HTML, the `<meta>` tag is used to provide metadata about the HTML document.
 * Metadata is information about the data within the document, rather than the content itself.
 * Metadata is typically used by browsers and search engines to interpret and display the document properly.
 * The `<meta>` tag is typically placed within the `<head>` element of an HTML document.
 *
 * <i>(ChatGPT, February 23, 2024)</i>
 */
class Meta(
    private val name: String,
    private val content: String,
) : AbstractTagBuilder<TagInterface>(), MetaInterface {
    override fun build(): TagInterface {
        return SelfClosingTag(
            "meta",
            attributes = Attributes(
                Attribute("name", name),
                Attribute("content", content)
            )
        )
    }
}