package info.scce.ktml

import info.scce.ktml.interfaces.ClassesInterface
import info.scce.ktml.interfaces.DivInterface
import info.scce.ktml.interfaces.TagInterface
import info.scce.ktml.util.AbstractTagBuilder
import info.scce.ktml.util.EmptyClasses
import info.scce.ktml.util.Tag

/**
 * In HTML, the `<div>` tag is a fundamental element used for creating divisions or sections within a web page.
 * It is a generic container that allows you to group together other HTML elements and apply styling or functionality
 * to them collectively.
 * The `<div>` tag itself does not have any inherent semantic meaning, but it is widely used for structuring and
 * organizing the layout of a webpage.
 *
 * <i>(ChatGPT, February 23, 2024)</i>
 */
class Div(
    private val attributes: Attributes,
    private val classes: ClassesInterface,
    private val children: List<TagInterface>,
) : AbstractTagBuilder<TagInterface>(), DivInterface {

    constructor(
        vararg children: TagInterface,
    ) : this(Attributes(), EmptyClasses(), children.toList())

    constructor(
        attributes: Attributes,
        vararg children: TagInterface,
    ) : this(attributes, EmptyClasses(), children.toList())

    constructor(
        attributes: Attributes,
        classes: ClassesInterface,
        vararg children: TagInterface,
    ) : this(attributes, classes, children.toList())

    constructor(
        classes: ClassesInterface,
        vararg children: TagInterface,
    ) : this(Attributes(), classes, children.toList())

    constructor(
        classes: ClassesInterface,
        children: List<TagInterface>,
    ) : this(Attributes(), classes, children)

    override fun build(): TagInterface {
        return Tag("div", children, Attributes(classes, attributes = attributes))
    }
}