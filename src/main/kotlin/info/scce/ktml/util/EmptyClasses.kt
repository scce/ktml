package info.scce.ktml.util

import info.scce.ktml.interfaces.ClassesInterface

class EmptyClasses : ClassesInterface {
    override fun notEmpty(): Boolean {
        return false
    }

    override fun string(): String {
        throw NotImplementedError()
    }
}