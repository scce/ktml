package info.scce.ktml.util

import info.scce.ktml.interfaces.AttributeInterface

abstract class AbstractAttributeDecorator(private val attribute: AttributeInterface) :
    AttributeInterface {

    override fun notEmpty(): Boolean {
        return attribute.notEmpty()
    }

    override fun string(): String {
        return attribute.string()
    }
}