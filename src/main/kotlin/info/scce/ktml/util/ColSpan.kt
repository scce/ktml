package info.scce.ktml.util

import info.scce.ktml.Attribute
import info.scce.ktml.interfaces.ColSpanInterface

class ColSpan(
    value: Int,
) : AbstractAttributeDecorator(Attribute("colspan", value)), ColSpanInterface
