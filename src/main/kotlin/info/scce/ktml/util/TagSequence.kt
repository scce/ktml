package info.scce.ktml.util

import info.scce.ktml.interfaces.TagInterface
import info.scce.ktml.interfaces.TagSequenceInterface

class TagSequence(
    private val tags: List<TagInterface>,
) : TagSequenceInterface {
    constructor(vararg tags: TagInterface) : this(tags.toList())
    constructor(
        tag: TagInterface,
        tags: List<TagInterface>,
    ) : this(listOf(tag, *tags.toTypedArray()))

    override fun string(): String {
        return tags.joinToString(separator = "") { it.string() }
    }

}