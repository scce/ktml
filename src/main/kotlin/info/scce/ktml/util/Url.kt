package info.scce.ktml.util

import info.scce.ktml.interfaces.UrlInterface

open class Url : UrlInterface {
    private val url: String

    constructor(url: String) {
        this.url = "/" + url.trimStart('/')
    }

    constructor(
        vararg pathSegments: String,
    ) : this(pathSegments.joinToString(separator = "/"))

    constructor(
        url: UrlInterface,
        suffix: Int,
    ) : this(url.string(), "$suffix")

    constructor(
        url: UrlInterface,
        suffix: String,
    ) : this(url.string(), suffix)

    constructor(
        url: UrlInterface,
        suffix: String,
        anchor: String,
    ) : this(url.string(), "$suffix#$anchor")

    constructor(
        url: UrlInterface,
        suffix: Int,
        anchor: String,
    ) : this(url.string(), "$suffix#$anchor")

    override fun string(): String {
        return url
    }
}
