package info.scce.ktml.util

import info.scce.ktml.Attributes
import info.scce.ktml.interfaces.TagInterface

class SelfClosingTag(
    private val name: String,
    private val attributes: Attributes,
) : TagInterface {

    override fun string(): String {
        return "<${name}${attributes.string()}>"
    }
}