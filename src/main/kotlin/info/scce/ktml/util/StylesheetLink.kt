package info.scce.ktml.util

import info.scce.ktml.Link
import info.scce.ktml.interfaces.TagInterface

class StylesheetLink(
    private val href: info.scce.ktml.interfaces.HrefInterface,
) : AbstractTagBuilder<TagInterface>(), info.scce.ktml.interfaces.LinkInterface {
    constructor(
        href: String,
    ) : this(
        Href(href)
    )

    override fun build(): TagInterface {
        return Link("stylesheet", href)
    }
}