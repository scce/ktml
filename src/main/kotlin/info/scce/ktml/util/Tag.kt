package info.scce.ktml.util

import info.scce.ktml.Attributes
import info.scce.ktml.interfaces.TagInterface

class Tag(
    private val name: String,
    private val child: TagInterface,
    private val attributes: Attributes,
) : TagInterface {

    constructor(
        name: String,
        children: List<TagInterface>,
        attributes: Attributes,
    ) : this(name, TagSequence(children), attributes)

    constructor(
        name: String,
        child: info.scce.ktml.interfaces.TextInterface,
        attributes: Attributes,
    ) : this(name, TagSequence(child), attributes)

    override fun string(): String {
        return "${SelfClosingTag(name, attributes).string()}${child.string()}</${name}>"
    }

}