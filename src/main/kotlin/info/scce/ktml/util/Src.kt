package info.scce.ktml.util

import info.scce.ktml.Attribute
import info.scce.ktml.interfaces.SrcInterface
import info.scce.ktml.interfaces.UrlInterface

class Src(
    url: String,
) : AbstractAttributeDecorator(
    Attribute("src", url)
), SrcInterface {
    constructor(url: UrlInterface) : this(url.string())
}
