package info.scce.ktml.util

import info.scce.ktml.Link
import info.scce.ktml.interfaces.LinkInterface

class Favicon(
    private val href: info.scce.ktml.interfaces.HrefInterface,
) : AbstractTagBuilder<LinkInterface>() {
    constructor(
        href: String,
    ) : this(
        Href(href)
    )

    override fun build(): LinkInterface {
        return Link(
            "icon",
            href
        )
    }

}