package info.scce.ktml.util

import info.scce.ktml.interfaces.TextInterface

open class EmptyText : TextInterface {
    override fun string(): String {
        return ""
    }
}