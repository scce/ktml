package info.scce.ktml.util

import info.scce.ktml.interfaces.TBodyInterface
import info.scce.ktml.interfaces.TFootInterface
import info.scce.ktml.interfaces.THeadInterface

class EmptyTableChild : THeadInterface, TBodyInterface, TFootInterface {
    override fun string(): String {
        return ""
    }
}