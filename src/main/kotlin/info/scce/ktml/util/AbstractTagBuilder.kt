package info.scce.ktml.util

import info.scce.ktml.interfaces.TagInterface

abstract class AbstractTagBuilder<T: TagInterface> {
    abstract fun build(): T
    fun string(): String {
        return this.build().string()
    }
}
