package info.scce.ktml.util

import info.scce.ktml.interfaces.AttributeInterface

class EmptyAttribute : AttributeInterface {
    override fun notEmpty(): Boolean {
        return false
    }

    override fun string(): String {
        throw NotImplementedError()
    }
}