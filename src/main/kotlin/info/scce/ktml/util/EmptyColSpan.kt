package info.scce.ktml.util

import info.scce.ktml.interfaces.ColSpanInterface

class EmptyColSpan : ColSpanInterface {
    override fun notEmpty(): Boolean {
        return false
    }

    override fun string(): String {
        throw NotImplementedError()
    }
}