package info.scce.ktml.util

import info.scce.ktml.Attribute
import info.scce.ktml.interfaces.HrefInterface
import info.scce.ktml.interfaces.StringInterface
import info.scce.ktml.interfaces.UrlInterface

class Href(
    url: String,
) : AbstractAttributeDecorator(Attribute("href", url)), HrefInterface {
    constructor(
        url: UrlInterface,
    ) : this(url.string())

    constructor(
        url: StringInterface,
    ) : this(url.string())

}