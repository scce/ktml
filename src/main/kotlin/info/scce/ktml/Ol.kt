package info.scce.ktml

import info.scce.ktml.interfaces.ClassesInterface
import info.scce.ktml.interfaces.LiInterface
import info.scce.ktml.interfaces.OlInterface
import info.scce.ktml.interfaces.TagInterface
import info.scce.ktml.util.AbstractTagBuilder
import info.scce.ktml.util.EmptyClasses
import info.scce.ktml.util.Tag


/**
 * In HTML, the `<ol>` tag is used to define an ordered list.
 * An ordered list is a list of items where each item is numbered sequentially.
 * The numbering typically starts at 1 and increments by one for each subsequent item.
 * By default, each list item in an ordered list is preceded by a number.
 *
 * <i>(ChatGPT, February 26, 2024)</i>
 */
class Ol(
    private val attributes: Attributes,
    private val classes: ClassesInterface,
    private val children: List<LiInterface>,
) : AbstractTagBuilder<TagInterface>(), OlInterface {
    constructor(
        vararg children: LiInterface,
    ) : this(Attributes(), EmptyClasses(), children.toList())

    constructor(
        attributes: Attributes,
        vararg children: LiInterface,
    ) : this(attributes, EmptyClasses(), children.toList())

    constructor(
        attributes: Attributes,
        classes: ClassesInterface,
        vararg children: LiInterface,
    ) : this(attributes, classes, children.toList())

    constructor(
        classes: ClassesInterface,
        vararg children: LiInterface,
    ) : this(Attributes(), classes, children.toList())

    constructor(
        classes: ClassesInterface,
        children: List<LiInterface>,
    ) : this(Attributes(), classes, children)

    override fun build(): TagInterface {
        return Tag("ol", children, Attributes(classes, attributes = attributes))
    }
}
