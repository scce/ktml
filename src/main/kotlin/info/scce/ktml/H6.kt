package info.scce.ktml

import info.scce.ktml.interfaces.*
import info.scce.ktml.util.AbstractTagBuilder
import info.scce.ktml.util.EmptyClasses
import info.scce.ktml.util.Tag

/**
 * In HTML, the `<h6>` tag is used to define a level 6 heading.
 * It is the lowest level of heading tag available in HTML.
 *
 * The `<h6>` tag is typically used to represent headings of the least importance compared to headings defined with
 * `<h1>`, `<h2>`, `<h3>`, `<h4>`, and `<h5>`. It's often used for sub-subsections or headings within a sub-subsection.
 *
 * <i>(ChatGPT, February 23, 2024)</i>
 */
class H6(
    // todo this should use AttributesInterface
    private val attributes: Attributes,
    private val classes: AttributeInterface,
    private val children: List<InlineTagInterface>,
) : AbstractTagBuilder<TagInterface>(), H6Interface {

    constructor(
        vararg children: InlineTagInterface,
    ) : this(Attributes(), EmptyClasses(), children.toList())

    constructor(
        attributes: Attributes,
        vararg children: InlineTagInterface,
    ) : this(attributes, EmptyClasses(), children.toList())

    constructor(
        attributes: Attributes,
        classes: ClassesInterface,
        vararg children: InlineTagInterface,
    ) : this(attributes, classes, children.toList())

    constructor(
        classes: ClassesInterface,
        vararg children: InlineTagInterface,
    ) : this(Attributes(), classes, children.toList())

    constructor(
        classes: ClassesInterface,
        children: List<InlineTagInterface>,
    ) : this(Attributes(), classes, children)

    override fun build(): TagInterface {
        return Tag("h6", children, Attributes(classes, attributes = attributes))
    }
}
