package info.scce.ktml

import info.scce.ktml.interfaces.ClassesInterface
import info.scce.ktml.interfaces.TableCellInterface
import info.scce.ktml.interfaces.TagInterface
import info.scce.ktml.interfaces.TrInterface
import info.scce.ktml.util.AbstractTagBuilder
import info.scce.ktml.util.EmptyClasses
import info.scce.ktml.util.Tag

/**
 * In HTML, the `<tr>` tag is used to define a row in an HTML table.
 * It stands for "table row" and is used to group together a set of table data cells (`<td>`) or table header cells
 * (`<th>`) that form a single row of content within the table.
 *
 * <i>(ChatGPT, February 26, 2024)</i>
 */
class Tr(
    private val attributes: Attributes,
    private val classes: ClassesInterface,
    private val children: List<TableCellInterface>,
) : AbstractTagBuilder<TagInterface>(), TrInterface {
    constructor(
        vararg children: TableCellInterface,
    ) : this(Attributes(), EmptyClasses(), children.toList())

    constructor(
        attributes: Attributes,
        vararg children: TableCellInterface,
    ) : this(attributes, EmptyClasses(), children.toList())

    constructor(
        attributes: Attributes,
        classes: ClassesInterface,
        vararg children: TableCellInterface,
    ) : this(attributes, classes, children.toList())

    constructor(
        classes: ClassesInterface,
        vararg children: TableCellInterface,
    ) : this(Attributes(), classes, children.toList())

    constructor(
        classes: ClassesInterface,
        children: List<TableCellInterface>,
    ) : this(Attributes(), classes, children)


    override fun build(): TagInterface {
        return Tag("tr", children, Attributes(classes, attributes = attributes))
    }
}
