package info.scce.ktml

import info.scce.ktml.interfaces.ClassesInterface
import info.scce.ktml.interfaces.InlineTagInterface
import info.scce.ktml.interfaces.PInterface
import info.scce.ktml.interfaces.TagInterface
import info.scce.ktml.util.AbstractTagBuilder
import info.scce.ktml.util.EmptyClasses
import info.scce.ktml.util.Tag

/**
 * In HTML, the `<p>` tag is used to define a paragraph.
 * It is a structural element that indicates a block of text.
 * The `<p>` tag is commonly used to structure and format textual content on a webpage.
 *
 * <i>(ChatGPT, February 26, 2024)</i>
 */
class P(
    private val attributes: Attributes,
    private val classes: ClassesInterface,
    private val children: List<InlineTagInterface>,
) : AbstractTagBuilder<TagInterface>(), PInterface {

    constructor(
        vararg children: InlineTagInterface,
    ) : this(Attributes(), EmptyClasses(), children.toList())

    constructor(
        attributes: Attributes,
        vararg children: InlineTagInterface,
    ) : this(attributes, EmptyClasses(), children.toList())

    constructor(
        attributes: Attributes,
        classes: ClassesInterface,
        vararg children: InlineTagInterface,
    ) : this(attributes, classes, children.toList())

    constructor(
        classes: ClassesInterface,
        vararg children: InlineTagInterface,
    ) : this(Attributes(), classes, children.toList())

    constructor(
        classes: ClassesInterface,
        children: List<InlineTagInterface>,
    ) : this(Attributes(), classes, children)

    override fun build(): TagInterface {
        return Tag("p", children, Attributes(classes, attributes = attributes))
    }

}