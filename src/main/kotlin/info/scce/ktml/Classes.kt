package info.scce.ktml

import info.scce.ktml.interfaces.ClassesInterface
import info.scce.ktml.util.AbstractAttributeDecorator

class Classes(
    private val classes: List<String>,
) : AbstractAttributeDecorator(Attribute("class", classes)), ClassesInterface {
    constructor(vararg classes: String) : this(classes.toList())

    constructor(
        classes: Classes,
        vararg additional: String,
    ) : this(classes.classes + additional.toList())
}