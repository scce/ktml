package info.scce.ktml

import info.scce.ktml.interfaces.ClassesInterface
import info.scce.ktml.interfaces.NavInterface
import info.scce.ktml.interfaces.TagInterface
import info.scce.ktml.util.AbstractTagBuilder
import info.scce.ktml.util.EmptyClasses
import info.scce.ktml.util.Tag

/**
 * In HTML, the `<nav>` tag is a semantic element used to define a section of navigation links within a webpage.
 * It's typically used to wrap around links that navigate to different parts of the website or provide navigation
 * options to users.
 * The `<nav>` element is part of HTML5 and provides a way to indicate to both browsers and search engines that the
 * enclosed content is related to navigation.
 *
 * <i>(ChatGPT, February 26, 2024)</i>
 */
class Nav(
    private val attributes: Attributes,
    private val classes: ClassesInterface,
    private val children: List<TagInterface>,
) : AbstractTagBuilder<TagInterface>(), NavInterface {

    constructor(
        vararg children: TagInterface,
    ) : this(Attributes(), EmptyClasses(), children.toList())

    constructor(
        attributes: Attributes,
        vararg children: TagInterface,
    ) : this(attributes, EmptyClasses(), children.toList())

    constructor(
        attributes: Attributes,
        classes: ClassesInterface,
        vararg children: TagInterface,
    ) : this(attributes, classes, children.toList())

    constructor(
        classes: ClassesInterface,
        vararg children: TagInterface,
    ) : this(Attributes(), classes, children.toList())

    constructor(
        classes: ClassesInterface,
        children: List<TagInterface>,
    ) : this(Attributes(), classes, children)

    override fun build(): TagInterface {
        return Tag("nav", children, Attributes(classes, attributes = attributes))
    }
}