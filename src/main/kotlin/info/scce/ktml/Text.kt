package info.scce.ktml

import info.scce.ktml.interfaces.StringInterface
import info.scce.ktml.interfaces.TextInterface

class Text(
    private val text: List<String>,
) : TextInterface {
    constructor(vararg text: String) : this(text.toList())
    constructor(vararg text: StringInterface) : this(text.toList().map { it.string() })
    constructor(vararg text: TextInterface) : this(text.toList().map { it.string() })

    override fun string(): String {
        return text.joinToString(separator = "")
    }
}