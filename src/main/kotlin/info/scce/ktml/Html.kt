package info.scce.ktml

import info.scce.ktml.interfaces.BodyInterface
import info.scce.ktml.interfaces.HeadInterface
import info.scce.ktml.interfaces.HtmlInterface
import info.scce.ktml.interfaces.TagInterface
import info.scce.ktml.util.AbstractTagBuilder
import info.scce.ktml.util.Tag
import info.scce.ktml.util.TagSequence

/**
 * In HTML, the <html> tag is the root element of an HTML document.
 * It wraps all the content of the document, including the <head> and <body> sections.
 * The <html> tag indicates the beginning of the HTML document and provides a container for all other HTML elements.
 *
 * <i>(ChatGPT, February 23, 2024)</i>
 */
class Html(
    private val head: HeadInterface,
    private val body: BodyInterface,
) : AbstractTagBuilder<TagInterface>(), HtmlInterface {
    override fun build(): TagInterface {
        return TagSequence(
            Doctype(),
            Tag(
                "html",
                TagSequence(
                    head,
                    body
                ),
                Attributes(
                    Attribute("lang", "en")
                )
            )
        )

    }

    override fun toString(): String {
        return this.string()
    }


}