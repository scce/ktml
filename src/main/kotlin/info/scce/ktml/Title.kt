package info.scce.ktml

import info.scce.ktml.interfaces.TagInterface
import info.scce.ktml.interfaces.TextInterface
import info.scce.ktml.interfaces.TitleInterface
import info.scce.ktml.util.AbstractTagBuilder
import info.scce.ktml.util.Tag

class Title(
    private val text: TextInterface,
) : AbstractTagBuilder<TagInterface>(), TitleInterface {
    override fun build(): TagInterface {
        return Tag("title", text, Attributes())
    }
}