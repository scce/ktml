package info.scce.ktml

import info.scce.ktml.interfaces.AttributeInterface
import info.scce.ktml.interfaces.StringInterface

class Attribute(
    private val key: String,
    private val value: List<String>,
) : AttributeInterface {
    constructor(
        key: String,
        value: Int,
    ) : this(key, value.toString())

    constructor(
        key: String,
        vararg value: String,
    ) : this(key, value.toList())

    constructor(
        key: String,
        value: StringInterface,
    ) : this(key, value.string())

    override fun string(): String {
        return "$key=\"${value.joinToString(" ")}\""
    }

    override fun notEmpty(): Boolean {
        return true
    }

}