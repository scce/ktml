package info.scce.ktml

import info.scce.ktml.interfaces.ClassesInterface
import info.scce.ktml.interfaces.ColSpanInterface
import info.scce.ktml.interfaces.TagInterface
import info.scce.ktml.interfaces.ThInterface
import info.scce.ktml.util.AbstractTagBuilder
import info.scce.ktml.util.EmptyClasses
import info.scce.ktml.util.EmptyColSpan
import info.scce.ktml.util.Tag

/**
 * In HTML, the `<th>` tag is used to define header cells in an HTML table.
 * It stands for "table header" and is typically used to represent column or row headings within a table.
 * Header cells are visually bold and centered by default, distinguishing them from regular data cells (`<td>`).
 *
 * <i>(ChatGPT, February 26, 2024)</i>
 */
class Th(
    private val attributes: Attributes,
    private val classes: ClassesInterface,
    private val colSpan: ColSpanInterface,
    private val children: List<TagInterface>,
) : AbstractTagBuilder<TagInterface>(), ThInterface {

    constructor(
        vararg children: TagInterface,
    ) : this(Attributes(), EmptyClasses(), EmptyColSpan(), children.toList())

    constructor(
        attributes: Attributes,
        vararg children: TagInterface,
    ) : this(attributes, EmptyClasses(), EmptyColSpan(), children.toList())

    constructor(
        classes: ClassesInterface,
        vararg children: TagInterface,
    ) : this(Attributes(), classes, EmptyColSpan(), children.toList())

    constructor(
        colSpan: ColSpanInterface,
        vararg children: TagInterface,
    ) : this(Attributes(), EmptyClasses(), colSpan, children.toList())

    constructor(
        attributes: Attributes,
        classes: ClassesInterface,
        vararg children: TagInterface,
    ) : this(attributes, classes, EmptyColSpan(), children.toList())

    constructor(
        attributes: Attributes,
        colSpan: ColSpanInterface,
        vararg children: TagInterface,
    ) : this(attributes, EmptyClasses(), colSpan, children.toList())

    constructor(
        classes: ClassesInterface,
        colSpan: ColSpanInterface,
        vararg children: TagInterface,
    ) : this(Attributes(), classes, colSpan, children.toList())

    constructor(
        attributes: Attributes,
        classes: ClassesInterface,
        colSpan: ColSpanInterface,
        vararg children: TagInterface,
    ) : this(attributes, classes, colSpan, children.toList())

    override fun build(): TagInterface {
        return Tag("th", children, Attributes(classes, colSpan, attributes = attributes))
    }
}
