package info.scce.ktml

import info.scce.ktml.interfaces.*
import info.scce.ktml.util.AbstractTagBuilder
import info.scce.ktml.util.EmptyClasses
import info.scce.ktml.util.Tag

/**
 * In HTML, the `<h2>` tag is used to define a level 2 heading.
 * It is part of a set of heading tags ranging from `<h1>` to `<h6>`, with `<h1>` representing the highest level of
 * heading and `<h6>` representing the lowest level.
 *
 * The `<h2>` tag is typically used to represent subheadings or titles that are of lesser importance compared to the
 * main title (which is often represented using the `<h1>` tag). `<h2>` headings are typically smaller and less
 * prominent than `<h1>` headings, but still larger and more noticeable than lower-level headings such as `<h3>`,
 * `<h4>`, and so on.
 *
 * <i>(ChatGPT, February 23, 2024)</i>
 */
class H2(
    // todo this should use AttributesInterface
    private val attributes: Attributes,
    private val classes: AttributeInterface,
    private val children: List<InlineTagInterface>,
) : AbstractTagBuilder<TagInterface>(), H1Interface {

    constructor(
        vararg children: InlineTagInterface,
    ) : this(Attributes(), EmptyClasses(), children.toList())

    constructor(
        attributes: Attributes,
        vararg children: InlineTagInterface,
    ) : this(attributes, EmptyClasses(), children.toList())

    constructor(
        attributes: Attributes,
        classes: ClassesInterface,
        vararg children: InlineTagInterface,
    ) : this(attributes, classes, children.toList())

    constructor(
        classes: ClassesInterface,
        vararg children: InlineTagInterface,
    ) : this(Attributes(), classes, children.toList())

    constructor(
        classes: ClassesInterface,
        children: List<InlineTagInterface>,
    ) : this(Attributes(), classes, children)

    override fun build(): TagInterface {
        return Tag("h2", children, Attributes(classes, attributes = attributes))
    }
}
