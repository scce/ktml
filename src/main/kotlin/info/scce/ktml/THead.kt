package info.scce.ktml

import info.scce.ktml.interfaces.ClassesInterface
import info.scce.ktml.interfaces.THeadInterface
import info.scce.ktml.interfaces.TagInterface
import info.scce.ktml.interfaces.TrInterface
import info.scce.ktml.util.AbstractTagBuilder
import info.scce.ktml.util.EmptyClasses
import info.scce.ktml.util.Tag

/**
 * In HTML, the `<thead>` tag is used to group the header rows (`<tr>`) in an HTML table.
 * It's a semantic element that indicates the beginning of the header section of a table.
 * The `<thead>` element is typically used in conjunction with the `<th>` (table header cell) element to define the
 * header cells of the table.
 *
 * <i>(ChatGPT, February 26, 2024)</i>
 */
class THead(
    private val attributes: Attributes,
    private val classes: ClassesInterface,
    private val children: List<TrInterface>,
) : AbstractTagBuilder<TagInterface>(), THeadInterface {

    constructor(
        vararg children: TrInterface,
    ) : this(Attributes(), EmptyClasses(), children.toList())

    constructor(
        attributes: Attributes,
        vararg children: TrInterface,
    ) : this(attributes, EmptyClasses(), children.toList())

    constructor(
        attributes: Attributes,
        classes: ClassesInterface,
        vararg children: TrInterface,
    ) : this(attributes, classes, children.toList())

    constructor(
        classes: ClassesInterface,
        vararg children: TrInterface,
    ) : this(Attributes(), classes, children.toList())

    constructor(
        classes: ClassesInterface,
        children: List<TrInterface>,
    ) : this(Attributes(), classes, children)

    override fun build(): TagInterface {
        return Tag("thead", children, Attributes(classes, attributes = attributes))
    }
}