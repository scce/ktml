package info.scce.ktml

import info.scce.ktml.interfaces.AttributeInterface
import info.scce.ktml.interfaces.BInterface
import info.scce.ktml.interfaces.TagInterface
import info.scce.ktml.interfaces.TextInterface
import info.scce.ktml.util.AbstractTagBuilder
import info.scce.ktml.util.EmptyAttribute
import info.scce.ktml.util.Tag

/**
 * In HTML, the `<b>` tag is used to apply bold formatting to text.
 * It is one of the formatting tags in HTML used for styling content.
 * When text is enclosed within `<b>` tags, it renders as bold text in most web browsers.
 * However, it's important to note that the `<b>` tag is a presentational element, meaning it defines how the text should
 * appear rather than what it represents semantically.
 *
 * <i>(ChatGPT, February 23, 2024)</i>
 */
class B(
    private val text: TextInterface,
    private val attributes: Attributes = Attributes(),
    private val classes: AttributeInterface = EmptyAttribute(),
) : AbstractTagBuilder<TagInterface>(), BInterface {
    override fun build(): TagInterface {
        return Tag("b", text, Attributes(classes, attributes = attributes))
    }
}