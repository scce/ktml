package info.scce.ktml

import info.scce.ktml.interfaces.*
import info.scce.ktml.util.*

/**
 * In HTML, the `<table>` tag is used to create a table.
 * Tables are used to display data in rows and columns, with each cell containing a piece of information.
 * Tables are a fundamental part of HTML for organizing and presenting structured data.
 *
 * <i>(ChatGPT, February 26, 2024)</i>
 */
class Table(
    private val attributes: Attributes,
    private val classes: AttributeInterface,
    private val tHead: THeadInterface,
    private val tBody: TBodyInterface,
    private val tFoot: TFootInterface,
) : AbstractTagBuilder<TagInterface>(), TableInterface {

    constructor(
        attributes: Attributes,
        tHead: THeadInterface,
    ) : this(attributes, EmptyClasses(), tHead, EmptyTableChild(), EmptyTableChild())

    constructor(
        attributes: Attributes,
        tBody: TBodyInterface,
    ) : this(attributes, EmptyClasses(), EmptyTableChild(), tBody, EmptyTableChild())

    constructor(
        attributes: Attributes,
        tFoot: TFootInterface,
    ) : this(attributes, EmptyClasses(), EmptyTableChild(), EmptyTableChild(), tFoot)

    constructor(
        classes: ClassesInterface,
        tHead: THeadInterface,
    ) : this(Attributes(), classes, tHead, EmptyTableChild(), EmptyTableChild())

    constructor(
        classes: ClassesInterface,
        tBody: TBodyInterface,
    ) : this(Attributes(), classes, EmptyTableChild(), tBody, EmptyTableChild())

    constructor(
        classes: ClassesInterface,
        tFoot: TFootInterface,
    ) : this(Attributes(), classes, EmptyTableChild(), EmptyTableChild(), tFoot)

    constructor(
        attributes: Attributes,
        classes: ClassesInterface,
        tHead: THeadInterface,
    ) : this(attributes, classes, tHead, EmptyTableChild(), EmptyTableChild())

    constructor(
        attributes: Attributes,
        classes: ClassesInterface,
        tBody: TBodyInterface,
    ) : this(attributes, classes, EmptyTableChild(), tBody, EmptyTableChild())

    constructor(
        attributes: Attributes,
        classes: ClassesInterface,
        tFoot: TFootInterface,
    ) : this(attributes, classes, EmptyTableChild(), EmptyTableChild(), tFoot)

    constructor(
        attributes: Attributes,
        tHead: THeadInterface,
        tBody: TBodyInterface,
    ) : this(attributes, EmptyClasses(), tHead, tBody, EmptyTableChild())

    constructor(
        attributes: Attributes,
        tHead: THeadInterface,
        tFoot: TFootInterface,
    ) : this(attributes, EmptyClasses(), tHead, EmptyTableChild(), tFoot)

    constructor(
        attributes: Attributes,
        tBody: TBodyInterface,
        tFoot: TFootInterface,
    ) : this(attributes, EmptyClasses(), EmptyTableChild(), tBody, tFoot)

    constructor(
        classes: ClassesInterface,
        tHead: THeadInterface,
        tBody: TBodyInterface,
    ) : this(Attributes(), classes, tHead, tBody, EmptyTableChild())

    constructor(
        classes: ClassesInterface,
        tHead: THeadInterface,
        tFoot: TFootInterface,
    ) : this(Attributes(), classes, tHead, EmptyTableChild(), tFoot)

    constructor(
        classes: ClassesInterface,
        tBody: TBodyInterface,
        tFoot: TFootInterface,
    ) : this(Attributes(), classes, EmptyTableChild(), tBody, tFoot)

    constructor(
        attributes: Attributes,
        tHead: THeadInterface,
        tBody: TBodyInterface,
        tFoot: TFootInterface,
    ) : this(attributes, EmptyClasses(), tHead, tBody, tFoot)

    constructor(
        classes: ClassesInterface,
        tHead: THeadInterface,
        tBody: TBodyInterface,
        tFoot: TFootInterface,
    ) : this(Attributes(), classes, tHead, tBody, tFoot)

    override fun build(): TagInterface {
        return Tag(
            "table",
            TagSequence(
                tHead,
                tBody,
                tFoot
            ),
            Attributes(classes, attributes = attributes)
        )
    }
}