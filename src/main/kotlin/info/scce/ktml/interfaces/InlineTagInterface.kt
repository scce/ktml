package info.scce.ktml.interfaces

/**
 * Represents an inline tag.
 *
 * An inline tag can be a child of a block tag or an inline tag
 *
 * Typical inline tags are:
 *
 * `<a>`
 * `<abbr>`
 * `<acronym>`
 * `<b>`
 * `<bdo>`
 * `<big>`
 * `<br>`
 * `<button>`
 * `<cite>`
 * `<code>`
 * `<dfn>`
 * `<em>`
 * `<i>`
 * `<img>`
 * `<input>`
 * `<kbd>`
 * `<label>`
 * `<map>`
 * `<object>`
 * `<output>`
 * `<q>`
 * `<samp>`
 * `<script>`
 * `<select>`
 * `<small>`
 * `<span>`
 * `<strong>`
 * `<sub>`
 * `<sup>`
 * `<textarea>`
 * `<time>`
 * `<tt>`
 * `<var>`
 *
 */
interface InlineTagInterface : TagInterface
