package info.scce.ktml.interfaces

interface HtmlInterface : BlockTagInterface {
    override fun toString(): String
}