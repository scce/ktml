package info.scce.ktml.interfaces

/**
 * Represents a block tag.
 *
 * A block tag can not be a child of an inline tag.
 *
 * Typical block tags are:
 *
 * `<address>`
 * `<article>`
 * `<aside>`
 * `<blockquote>`
 * `<canvas>`
 * `<dd>`
 * `<div>`
 * `<dl>`
 * `<dt>`
 * `<fieldset>`
 * `<figcaption>`
 * `<figure>`
 * `<footer>`
 * `<form>`
 * `<h1>`-`<h6>`
 * `<header>`
 * `<hr>`
 * `<li>`
 * `<main>`
 * `<nav>`
 * `<noscript>`
 * `<ol>`
 * `<p>`
 * `<pre>`
 * `<section>`
 * `<table>`
 * `<tfoot>`
 * `<ul>`
 * `<video>`
 *
 */
interface BlockTagInterface : TagInterface
