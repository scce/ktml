package info.scce.ktml.interfaces

interface AttributeInterface {
    fun notEmpty(): Boolean
    fun string(): String
}
