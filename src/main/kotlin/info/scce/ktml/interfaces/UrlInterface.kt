package info.scce.ktml.interfaces

interface UrlInterface {
    fun string(): String
}
