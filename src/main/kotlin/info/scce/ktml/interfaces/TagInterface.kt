package info.scce.ktml.interfaces

interface TagInterface {
    fun string(): String
}
