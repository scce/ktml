package info.scce.ktml

import info.scce.ktml.interfaces.AttributeInterface
import info.scce.ktml.interfaces.HrInterface
import info.scce.ktml.interfaces.TagInterface
import info.scce.ktml.util.AbstractTagBuilder
import info.scce.ktml.util.EmptyClasses
import info.scce.ktml.util.SelfClosingTag

/**
 * In HTML, the `<hr>` tag is used to create a horizontal rule, also known as a thematic break or divider, in a webpage.
 * It is a self-closing tag, meaning it does not require a closing tag.
 *
 * <i>(ChatGPT, February 26, 2024)</i>
 */
class Hr(
    private val attributes: Attributes,
    private val classes: AttributeInterface,
) : AbstractTagBuilder<TagInterface>(), HrInterface {

    constructor() : this(Attributes(), EmptyClasses())

    constructor(
        classes: AttributeInterface,
    ) : this(Attributes(), classes)

    constructor(
        attributes: Attributes,
    ) : this(attributes, EmptyClasses())

    override fun build(): TagInterface {
        return SelfClosingTag("hr", Attributes(classes, attributes = attributes))
    }
}
