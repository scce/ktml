package info.scce.ktml

import info.scce.ktml.interfaces.ClassesInterface
import info.scce.ktml.interfaces.InlineTagInterface
import info.scce.ktml.interfaces.SpanInterface
import info.scce.ktml.interfaces.TagInterface
import info.scce.ktml.util.AbstractTagBuilder
import info.scce.ktml.util.EmptyClasses
import info.scce.ktml.util.Tag

/**
 * In HTML, the `<span>` tag is an inline element used to apply styling or manipulate specific portions of text within
 * a larger block of content. Unlike block-level elements (e.g., `<div>`, `<p>`), the `<span>` tag does not add any
 * line breaks before or after the element, but rather applies styles or behavior directly to the content within it.
 *
 * <i>(ChatGPT, February 26, 2024)</i>
 */
class Span(
    private val attributes: Attributes,
    private val classes: ClassesInterface,
    private val children: List<InlineTagInterface>,
) : AbstractTagBuilder<TagInterface>(), SpanInterface {
    constructor(
        vararg children: InlineTagInterface,
    ) : this(Attributes(), EmptyClasses(), children.toList())

    constructor(
        attributes: Attributes,
        vararg children: InlineTagInterface,
    ) : this(attributes, EmptyClasses(), children.toList())

    constructor(
        attributes: Attributes,
        classes: ClassesInterface,
        vararg children: InlineTagInterface,
    ) : this(attributes, classes, children.toList())

    constructor(
        classes: ClassesInterface,
        vararg children: InlineTagInterface,
    ) : this(Attributes(), classes, children.toList())

    constructor(
        classes: ClassesInterface,
        children: List<InlineTagInterface>,
    ) : this(Attributes(), classes, children)

    override fun build(): TagInterface {
        return Tag("span", children, Attributes(classes, attributes = attributes))
    }
}