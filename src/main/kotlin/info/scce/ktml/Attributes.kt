package info.scce.ktml

import info.scce.ktml.interfaces.AttributeInterface
import info.scce.ktml.interfaces.AttributesInterface

class Attributes(
    private val attributes: List<AttributeInterface>,
) : AttributesInterface {
    constructor(
        vararg attribute: AttributeInterface,
    ) : this(attribute.toList())

    constructor(
        vararg attribute: AttributeInterface,
        attributes: Attributes,
    ) : this(attribute.toList() + attributes.attributes)

    override fun string(): String {
        val attributes = this.attributes.filter { it.notEmpty() }
        if (attributes.isEmpty()) {
            return ""
        }
        return attributes.joinToString(separator = " ", prefix = " ") { it.string() }
    }
}