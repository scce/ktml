package info.scce.ktml

import info.scce.ktml.interfaces.HrefInterface
import info.scce.ktml.interfaces.LinkInterface
import info.scce.ktml.interfaces.TagInterface
import info.scce.ktml.util.AbstractTagBuilder
import info.scce.ktml.util.Href
import info.scce.ktml.util.SelfClosingTag

/**
 * In HTML, the "link" tag in the header section typically refers to the `<link>` element used to link external
 * resources such as stylesheets, favicons, or alternate versions of the document.
 * The `<link>` tag is commonly used within the `<head>` section of an HTML document to specify relationships between
 * the current document and external resources.
 *
 * <i>(ChatGPT, February 23, 2024)</i>
 */
class Link(
    private val rel: String,
    private val href: HrefInterface,
) : AbstractTagBuilder<TagInterface>(), LinkInterface {
    constructor(
        rel: String,
        href: String,
    ) : this(
        rel,
        Href(
            href
        )
    )

    override fun build(): TagInterface {
        return SelfClosingTag(
            "link",
            Attributes(
                Attribute(
                    "rel",
                    rel
                ),
                href,
            )
        )
    }
}