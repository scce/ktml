package info.scce.ktml

import info.scce.ktml.interfaces.ClassesInterface
import info.scce.ktml.interfaces.ColSpanInterface
import info.scce.ktml.interfaces.TagInterface
import info.scce.ktml.interfaces.TdInterface
import info.scce.ktml.util.AbstractTagBuilder
import info.scce.ktml.util.EmptyClasses
import info.scce.ktml.util.EmptyColSpan
import info.scce.ktml.util.Tag

/**
 * In HTML, the `<td>` tag is used to define a single cell (data cell) within an HTML table.
 * It stands for "table data" and is used to contain data (such as text, images, links, etc.) within a row (`<tr>`) of
 * a table.
 *
 * Data cells (`<td>` elements) are typically used to display tabular data, such as information in a database,
 * spreadsheet, or any other structured data source.
 * Each `<td>` element represents one piece of data within a row of the table.
 * The number of `<td>` elements in each row should match the number of columns defined in the table header (`<th>`) or
 * in the first row of the table if no `<thead>` section is used.
 *
 * <i>(ChatGPT, February 26, 2024)</i>
 */
class Td(
    private val attributes: Attributes,
    private val classes: ClassesInterface,
    private val colSpan: ColSpanInterface,
    private val children: List<TagInterface>,
) : AbstractTagBuilder<TagInterface>(), TdInterface {

    constructor(
        vararg children: TagInterface,
    ) : this(Attributes(), EmptyClasses(), EmptyColSpan(), children.toList())

    constructor(
        attributes: Attributes,
        vararg children: TagInterface,
    ) : this(attributes, EmptyClasses(), EmptyColSpan(), children.toList())

    constructor(
        classes: ClassesInterface,
        vararg children: TagInterface,
    ) : this(Attributes(), classes, EmptyColSpan(), children.toList())

    constructor(
        colSpan: ColSpanInterface,
        vararg children: TagInterface,
    ) : this(Attributes(), EmptyClasses(), colSpan, children.toList())

    constructor(
        attributes: Attributes,
        classes: ClassesInterface,
        vararg children: TagInterface,
    ) : this(attributes, classes, EmptyColSpan(), children.toList())

    constructor(
        attributes: Attributes,
        colSpan: ColSpanInterface,
        vararg children: TagInterface,
    ) : this(attributes, EmptyClasses(), colSpan, children.toList())

    constructor(
        classes: ClassesInterface,
        colSpan: ColSpanInterface,
        vararg children: TagInterface,
    ) : this(Attributes(), classes, colSpan, children.toList())

    constructor(
        attributes: Attributes,
        classes: ClassesInterface,
        colSpan: ColSpanInterface,
        vararg children: TagInterface,
    ) : this(attributes, classes, colSpan, children.toList())

    override fun build(): TagInterface {
        return Tag("td", children, Attributes(classes, colSpan, attributes = attributes))
    }
}
