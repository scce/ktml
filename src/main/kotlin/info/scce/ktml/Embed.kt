package info.scce.ktml

import info.scce.ktml.interfaces.ClassesInterface
import info.scce.ktml.interfaces.EmbedInterface
import info.scce.ktml.interfaces.SrcInterface
import info.scce.ktml.interfaces.TagInterface
import info.scce.ktml.util.AbstractTagBuilder
import info.scce.ktml.util.EmptyClasses
import info.scce.ktml.util.SelfClosingTag

/**
 * In HTML, the `<embed>` tag is used to embed external content, typically multimedia content such as audio, video, or
 * interactive content, into an HTML document.
 * The `<embed>` tag was originally introduced in HTML 4.01 to provide a way to include external plugins or applets
 * within a webpage.
 * However, its usage has decreased in favor of more modern approaches such as the `<audio>`, `<video>`, and
 * `<iframe>` tags.
 *
 * <i>(ChatGPT, February 23, 2024)</i>
 */
class Embed(
    private val attributes: Attributes,
    private val classes: ClassesInterface,
    private val src: SrcInterface,
    private val type: String,
) : AbstractTagBuilder<TagInterface>(), EmbedInterface {

    constructor(
        src: SrcInterface,
        type: String,
    ) : this(Attributes(), EmptyClasses(), src, type)

    constructor(
        attributes: Attributes,
        src: SrcInterface,
        type: String,
    ) : this(attributes, EmptyClasses(), src, type)

    constructor(
        classes: ClassesInterface,
        src: SrcInterface,
        type: String,
    ) : this(Attributes(), classes, src, type)

    override fun build(): TagInterface {
        return SelfClosingTag(
            "embed",
            Attributes(
                src,
                Attribute("type", type),
                classes,
                attributes = attributes
            )
        )
    }
}