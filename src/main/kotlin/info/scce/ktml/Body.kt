package info.scce.ktml

import info.scce.ktml.interfaces.BodyInterface
import info.scce.ktml.interfaces.ClassesInterface
import info.scce.ktml.interfaces.TagInterface
import info.scce.ktml.util.AbstractTagBuilder
import info.scce.ktml.util.EmptyClasses
import info.scce.ktml.util.Tag

/**
 * In HTML, the `<body>` tag is used to define the content of the document, including all the visible elements that users
 * see when they visit a web page.
 * It is one of the structural tags in HTML and is required in every HTML document.
 *
 * The `<body>` tag contains all the content that is displayed in the browser window, including text, images, links,
 * multimedia elements, forms, and more.
 * It typically follows the `<head>` tag in an HTML document and precedes the closing `</html>` tag.
 *
 * <i>(ChatGPT, February 23, 2024)</i>
 */
class Body(
    private val attributes: Attributes,
    private val classes: ClassesInterface,
    private val children: List<TagInterface>,
) : AbstractTagBuilder<TagInterface>(), BodyInterface {

    constructor(
        vararg children: TagInterface,
    ) : this(Attributes(), EmptyClasses(), children.toList())

    constructor(
        attributes: Attributes,
        vararg children: TagInterface,
    ) : this(attributes, EmptyClasses(), children.toList())

    constructor(
        attributes: Attributes,
        classes: ClassesInterface,
        vararg children: TagInterface,
    ) : this(attributes, classes, children.toList())

    constructor(
        classes: ClassesInterface,
        vararg children: TagInterface,
    ) : this(Attributes(), classes, children.toList())

    constructor(
        classes: ClassesInterface,
        children: List<TagInterface>,
    ) : this(Attributes(), classes, children)

    override fun build(): TagInterface {
        return Tag("body", children, Attributes(classes, attributes = attributes))
    }
}