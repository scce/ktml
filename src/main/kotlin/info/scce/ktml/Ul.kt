package info.scce.ktml

import info.scce.ktml.interfaces.ClassesInterface
import info.scce.ktml.interfaces.LiInterface
import info.scce.ktml.interfaces.TagInterface
import info.scce.ktml.interfaces.UlInterface
import info.scce.ktml.util.AbstractTagBuilder
import info.scce.ktml.util.EmptyClasses
import info.scce.ktml.util.Tag

/**
 * In HTML, the `<ul>` tag is used to define an unordered list.
 * An unordered list is a list of items where the order of the items does not matter, and typically each item is
 * preceded by a bullet point.
 *
 * <i>(ChatGPT, February 23, 2024)</i>
 */
class Ul(
    private val attributes: Attributes,
    private val classes: ClassesInterface,
    private val children: List<LiInterface>,
) : AbstractTagBuilder<TagInterface>(), UlInterface {
    constructor(
        vararg children: LiInterface,
    ) : this(Attributes(), EmptyClasses(), children.toList())

    constructor(
        attributes: Attributes,
        vararg children: LiInterface,
    ) : this(attributes, EmptyClasses(), children.toList())

    constructor(
        attributes: Attributes,
        classes: ClassesInterface,
        vararg children: LiInterface,
    ) : this(attributes, classes, children.toList())

    constructor(
        classes: ClassesInterface,
        vararg children: LiInterface,
    ) : this(Attributes(), classes, children.toList())

    constructor(
        classes: ClassesInterface,
        children: List<LiInterface>,
    ) : this(Attributes(), classes, children)

    override fun build(): TagInterface {
        return Tag("ul", children, Attributes(classes, attributes = attributes))
    }
}
