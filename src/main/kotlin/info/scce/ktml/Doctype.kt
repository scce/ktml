package info.scce.ktml

import info.scce.ktml.interfaces.TagInterface

/**
 * In HTML, the `<!DOCTYPE>` declaration, often referred to as the doctype declaration, is not a tag but rather an
 * instruction that specifies the version of HTML (or XHTML) used in a document.
 * It must appear at the very beginning of an HTML document, before any other content, including the `<html>` tag.
 *
 * The purpose of the `<!DOCTYPE>` declaration is to inform web browsers and other parsers about the version of HTML
 * being used in the document, as well as the document type definition (DTD) that describes the structure and syntax
 * rules for that version of HTML. This helps ensure that the document is parsed and rendered correctly by browsers
 * and other tools.
 *
 * <i>(ChatGPT, February 23, 2024)</i>
 */
class Doctype : TagInterface {
    override fun string(): String {
        return "<!DOCTYPE html>"
    }
}