package info.scce.ktml.fake

import info.scce.ktml.interfaces.StringInterface

class FakeStringInterface(private val value: String) : StringInterface {
    override fun string(): String {
        return value
    }
}
