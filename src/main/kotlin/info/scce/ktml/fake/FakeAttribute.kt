package info.scce.ktml.fake

import info.scce.ktml.interfaces.AttributeInterface

class FakeAttribute(private val value: String = "fakeAttribute") : AttributeInterface {
    override fun notEmpty(): Boolean {
        return true
    }

    override fun string(): String {
        return value
    }
}