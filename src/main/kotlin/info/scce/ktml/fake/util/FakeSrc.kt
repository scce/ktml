package info.scce.ktml.fake.util

import info.scce.ktml.interfaces.SrcInterface

class FakeSrc(
    private val value: String = "fakeSrc",
) : SrcInterface {
    override fun notEmpty(): Boolean {
        return true
    }

    override fun string(): String {
        return value
    }
}
