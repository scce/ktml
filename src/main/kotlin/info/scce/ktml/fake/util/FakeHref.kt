package info.scce.ktml.fake.util

import info.scce.ktml.interfaces.HrefInterface

class FakeHref(
    private val value: String = "fakeHref",
) : HrefInterface {
    override fun notEmpty(): Boolean {
        return true
    }

    override fun string(): String {
        return value
    }

}