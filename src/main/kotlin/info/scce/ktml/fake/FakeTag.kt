package info.scce.ktml.fake

import info.scce.ktml.Attributes
import info.scce.ktml.interfaces.TagInterface
import info.scce.ktml.util.AbstractTagBuilder
import info.scce.ktml.util.SelfClosingTag

open class FakeTag(private val value: String = "tag") : AbstractTagBuilder<TagInterface>(),
    TagInterface {
    override fun build(): TagInterface {
        return SelfClosingTag(value, Attributes())
    }

}

