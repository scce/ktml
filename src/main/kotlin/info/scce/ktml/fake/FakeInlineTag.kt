package info.scce.ktml.fake

import info.scce.ktml.Attributes
import info.scce.ktml.interfaces.InlineTagInterface
import info.scce.ktml.interfaces.TagInterface
import info.scce.ktml.util.AbstractTagBuilder
import info.scce.ktml.util.SelfClosingTag

open class FakeInlineTag(private val value: String = "tag") :
    AbstractTagBuilder<TagInterface>(), InlineTagInterface {
    override fun build(): TagInterface {
        return SelfClosingTag(value, Attributes())
    }

}

