package info.scce.ktml.fake

import info.scce.ktml.interfaces.ColSpanInterface

class FakeColSpan(private val value: String = "fakeColSpan") : ColSpanInterface {
    override fun notEmpty(): Boolean {
        return true;
    }

    override fun string(): String {
        return value
    }

}
