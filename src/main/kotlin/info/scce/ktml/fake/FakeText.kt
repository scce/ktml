package info.scce.ktml.fake

import info.scce.ktml.interfaces.TextInterface

class FakeText(private val text: String = "text") : TextInterface {
    override fun string(): String {
        return text
    }
}