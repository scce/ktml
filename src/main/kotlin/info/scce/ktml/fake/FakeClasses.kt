package info.scce.ktml.fake

import info.scce.ktml.interfaces.ClassesInterface

class FakeClasses(private val value: String = "fakeClasses") : ClassesInterface {
    override fun notEmpty(): Boolean {
        return true
    }

    override fun string(): String {
        return value
    }
}