package info.scce.ktml

import info.scce.ktml.interfaces.ClassesInterface
import info.scce.ktml.interfaces.TBodyInterface
import info.scce.ktml.interfaces.TagInterface
import info.scce.ktml.interfaces.TrInterface
import info.scce.ktml.util.AbstractTagBuilder
import info.scce.ktml.util.EmptyClasses
import info.scce.ktml.util.Tag

/**
 * In HTML, the `<tbody>` tag is used to define the body section of an HTML table.
 * It stands for "table body" and is used to group the main content rows (`<tr>`) in a table.
 * The `<tbody>` element is typically placed after the `<thead>` (header) section and before the `<tfoot>` (footer)
 * section, if present, within the `<table>` element.
 *
 * <i>(ChatGPT, February 26, 2024)</i>
 */
class TBody(
    private val attributes: Attributes,
    private val classes: ClassesInterface,
    private val children: List<TrInterface>,
) : AbstractTagBuilder<TagInterface>(), TBodyInterface {

    constructor(
        vararg children: TrInterface,
    ) : this(Attributes(), EmptyClasses(), children.toList())

    constructor(
        attributes: Attributes,
        vararg children: TrInterface,
    ) : this(attributes, EmptyClasses(), children.toList())

    constructor(
        attributes: Attributes,
        classes: ClassesInterface,
        vararg children: TrInterface,
    ) : this(attributes, classes, children.toList())

    constructor(
        classes: ClassesInterface,
        vararg children: TrInterface,
    ) : this(Attributes(), classes, children.toList())

    constructor(
        classes: ClassesInterface,
        children: List<TrInterface>,
    ) : this(Attributes(), classes, children)

    override fun build(): TagInterface {
        return Tag("tbody", children, Attributes(classes, attributes = attributes))
    }
}