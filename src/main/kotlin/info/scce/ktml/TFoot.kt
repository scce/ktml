package info.scce.ktml

import info.scce.ktml.interfaces.ClassesInterface
import info.scce.ktml.interfaces.TFootInterface
import info.scce.ktml.interfaces.TagInterface
import info.scce.ktml.interfaces.TrInterface
import info.scce.ktml.util.AbstractTagBuilder
import info.scce.ktml.util.EmptyClasses
import info.scce.ktml.util.Tag

/**
 * In HTML, the `<tfoot>` tag is used to define a footer section for an HTML table.
 * It stands for "table footer" and is used to group footer rows (`<tr>`) in a table.
 * The `<tfoot>` element is typically placed after the `<tbody>` (body) section and before the closing </table> tag.
 *
 * <i>(ChatGPT, February 26, 2024)</i>
 */
class TFoot(
    private val attributes: Attributes,
    private val classes: ClassesInterface,
    private val children: List<TrInterface>,
) : AbstractTagBuilder<TagInterface>(), TFootInterface {

    constructor(
        vararg children: TrInterface,
    ) : this(Attributes(), EmptyClasses(), children.toList())

    constructor(
        attributes: Attributes,
        vararg children: TrInterface,
    ) : this(attributes, EmptyClasses(), children.toList())

    constructor(
        attributes: Attributes,
        classes: ClassesInterface,
        vararg children: TrInterface,
    ) : this(attributes, classes, children.toList())

    constructor(
        classes: ClassesInterface,
        vararg children: TrInterface,
    ) : this(Attributes(), classes, children.toList())

    constructor(
        classes: ClassesInterface,
        children: List<TrInterface>,
    ) : this(Attributes(), classes, children)

    override fun build(): TagInterface {
        return Tag("tfoot", children, Attributes(classes, attributes = attributes))
    }
}
