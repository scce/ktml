package info.scce.ktml

import info.scce.ktml.interfaces.*
import info.scce.ktml.util.AbstractTagBuilder
import info.scce.ktml.util.EmptyClasses
import info.scce.ktml.util.Tag

/**
 * In HTML, the `<h1>` tag is used to define the most important heading or title on a webpage.
 * It is part of a set of heading tags ranging from `<h1>` to `<h6>`, where `<h1>` represents the highest level of
 * heading and `<h6>` represents the lowest level of heading.
 *
 * The `<h1>` tag is typically used for the main title of the webpage, which often describes the overall topic or purpose
 * of the page.
 * It's important to use `<h1>` tags for headings that are the most significant in terms of content hierarchy and
 * importance.
 *
 * <i>(ChatGPT, February 23, 2024)</i>
 */
class H1(
    // todo this should use AttributesInterface
    private val attributes: Attributes,
    private val classes: AttributeInterface,
    private val children: List<InlineTagInterface>,
) : AbstractTagBuilder<TagInterface>(), H1Interface {

    constructor(
        vararg children: InlineTagInterface,
    ) : this(Attributes(), EmptyClasses(), children.toList())

    constructor(
        attributes: Attributes,
        vararg children: InlineTagInterface,
    ) : this(attributes, EmptyClasses(), children.toList())

    constructor(
        attributes: Attributes,
        classes: ClassesInterface,
        vararg children: InlineTagInterface,
    ) : this(attributes, classes, children.toList())

    constructor(
        classes: ClassesInterface,
        vararg children: InlineTagInterface,
    ) : this(Attributes(), classes, children.toList())

    constructor(
        classes: ClassesInterface,
        children: List<InlineTagInterface>,
    ) : this(Attributes(), classes, children)

    override fun build(): TagInterface {
        return Tag("h1", children, Attributes(classes, attributes = attributes))
    }
}