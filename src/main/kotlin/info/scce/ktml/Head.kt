package info.scce.ktml

import info.scce.ktml.interfaces.ClassesInterface
import info.scce.ktml.interfaces.HeadInterface
import info.scce.ktml.interfaces.TagInterface
import info.scce.ktml.interfaces.TitleInterface
import info.scce.ktml.util.AbstractTagBuilder
import info.scce.ktml.util.EmptyClasses
import info.scce.ktml.util.Tag
import info.scce.ktml.util.TagSequence

/**
 * In HTML, the `<head>` tag is a container element that holds metadata and other information about the HTML document.
 * It is part of the structural components of an HTML document and is placed between the opening `<html>` tag and the
 * opening `<body>` tag.
 * The `<head>` element contains various types of metadata and resources that are essential for the proper functioning
 * and presentation of the HTML document.
 *
 * <i>(ChatGPT, February 23, 2024)</i>
 */
class Head(
    private val attributes: Attributes,
    private val classes: ClassesInterface,
    private val title: TitleInterface,
    private val children: List<TagInterface>,
) : AbstractTagBuilder<TagInterface>(), HeadInterface {
    constructor(
        title: TitleInterface,
        vararg children: TagInterface,
    ) : this(Attributes(), EmptyClasses(), title, children.toList())

    constructor(
        attributes: Attributes,
        title: TitleInterface,
        vararg children: TagInterface,
    ) : this(attributes, EmptyClasses(), title, children.toList())

    constructor(
        attributes: Attributes,
        classes: ClassesInterface,
        title: TitleInterface,
        vararg children: TagInterface,
    ) : this(attributes, classes, title, children.toList())

    constructor(
        classes: ClassesInterface,
        title: TitleInterface,
        vararg children: TagInterface,
    ) : this(Attributes(), classes, title, children.toList())

    constructor(
        classes: ClassesInterface,
        title: TitleInterface,
        children: List<TagInterface>,
    ) : this(Attributes(), classes, title, children)

    override fun build(): TagInterface {
        return Tag("head", TagSequence(title, children), Attributes(classes, attributes = attributes))
    }
}
