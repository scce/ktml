package info.scce.ktml

import info.scce.ktml.fake.FakeBody
import info.scce.ktml.fake.FakeHead
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class HtmlTest {

    @Test
    fun test() {
        //then
        assertEquals(
            """<!DOCTYPE html><html lang="en"><head><body></html>""",
            Html(
                //given
                FakeHead(),
                FakeBody()
            )
                //when
                .build().string()
        )
    }

    @Test
    fun integration() {
        //then
        assertEquals(
            """<!DOCTYPE html><html lang="en"><head><title>Example</title></head><body>Hello World!</body></html>""",
            Html(
                Head(
                    Title(
                        Text("Example")
                    )
                ),
                Body(
                    Text("Hello World!")
                )
            ).toString()
        )
    }
}