package info.scce.ktml

import info.scce.ktml.fake.FakeAttribute
import info.scce.ktml.fake.FakeClasses
import info.scce.ktml.fake.FakeInlineTag
import org.junit.jupiter.api.Test

class SpanTest {

    @Test
    fun testSecondConstructor() {
        //then
        assertEquals(
            "<span><tag></span>",
            //when
            Span(
                //given
                FakeInlineTag()
            )
        )
    }

    @Test
    fun testSecondConstructorEmspanty() {
        //then
        assertEquals(
            """<span></span>""",
            //when
            Span(
                //given
            )
        )
    }

    @Test
    fun testThirdConstructor() {
        //then
        assertEquals(
            """<span fakeAttribute></span>""",
            //when
            Span(
                //given
                Attributes(FakeAttribute())
            )
        )
    }

    @Test
    fun testThirdConstructorNamedspanarameter() {
        //then
        assertEquals(
            """<span fakeAttribute></span>""",
            //when
            Span(
                //given
                attributes = Attributes(FakeAttribute())
            )
        )
    }

    @Test
    fun testFourthConstructor() {
        //then
        assertEquals(
            """<span fakeClasses fakeAttribute></span>""",
            //when
            Span(
                //given
                Attributes(FakeAttribute()),
                FakeClasses()
            )
        )
    }

    @Test
    fun testFourthConstructorNamedspanarameters() {
        //then
        assertEquals(
            """<span fakeClasses fakeAttribute></span>""",
            //when
            Span(
                //given
                attributes = Attributes(FakeAttribute()),
                classes = FakeClasses()
            )
        )
    }

    @Test
    fun testFourthConstructorMixingspanarameters() {
        //then
        assertEquals(
            """<span fakeClasses fakeAttribute></span>""",
            //when
            Span(
                //given
                attributes = Attributes(FakeAttribute()),
                FakeClasses()
            )
        )
    }

    @Test
    fun testFifthConstructor() {
        //then
        assertEquals(
            """<span fakeClasses><tag></span>""",
            //when
            Span(
                //given
                FakeClasses(),
                FakeInlineTag(),
            )
        )
    }

    @Test
    fun testFifthConstructorMixedspanarameters() {
        //then
        assertEquals(
            """<span fakeClasses><tag></span>""",
            //when
            Span(
                //given
                classes = FakeClasses(),
                FakeInlineTag()
            )
        )
    }

    @Test
    fun testSixthConstructorMixedspanarameters() {
        //then
        assertEquals(
            """<span fakeClasses><tag></span>""",
            //when
            Span(
                //given
                classes = FakeClasses(),
                listOf(FakeInlineTag())
            )
        )
    }
}