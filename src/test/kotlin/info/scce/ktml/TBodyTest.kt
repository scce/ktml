package info.scce.ktml

import info.scce.ktml.fake.FakeAttribute
import info.scce.ktml.fake.FakeClasses
import info.scce.ktml.fake.FakeTr
import org.junit.jupiter.api.Test

class TBodyTest {
    @Test
    fun testSecondConstructor() {
        //then
        assertEquals(
            "<tbody><tr></tbody>",
            //when
            TBody(
                //given
                FakeTr()
            )
        )
    }

    @Test
    fun testSecondConstructorEmpty() {
        //then
        assertEquals(
            """<tbody></tbody>""",
            //when
            TBody(
                //given
            )
        )
    }

    @Test
    fun testThirdConstructor() {
        //then
        assertEquals(
            """<tbody fakeAttribute></tbody>""",
            //when
            TBody(
                //given
                Attributes(FakeAttribute())
            )
        )
    }

    @Test
    fun testThirdConstructorNamedParameter() {
        //then
        assertEquals(
            """<tbody fakeAttribute></tbody>""",
            //when
            TBody(
                //given
                attributes = Attributes(FakeAttribute())
            )
        )
    }

    @Test
    fun testFourthConstructor() {
        //then
        assertEquals(
            """<tbody fakeClasses fakeAttribute></tbody>""",
            //when
            TBody(
                //given
                Attributes(FakeAttribute()),
                FakeClasses()
            )
        )
    }

    @Test
    fun testFourthConstructorNamedParameters() {
        //then
        assertEquals(
            """<tbody fakeClasses fakeAttribute></tbody>""",
            //when
            TBody(
                //given
                attributes = Attributes(FakeAttribute()),
                classes = FakeClasses()
            )
        )
    }

    @Test
    fun testFourthConstructorMixingParameters() {
        //then
        assertEquals(
            """<tbody fakeClasses fakeAttribute></tbody>""",
            //when
            TBody(
                //given
                attributes = Attributes(FakeAttribute()),
                FakeClasses()
            )
        )
    }

    @Test
    fun testFifthConstructor() {
        //then
        assertEquals(
            """<tbody fakeClasses><tr></tbody>""",
            //when
            TBody(
                //given
                FakeClasses(),
                FakeTr(),
            )
        )
    }

    @Test
    fun testFifthConstructorMixedParameters() {
        //then
        assertEquals(
            """<tbody fakeClasses><tr></tbody>""",
            //when
            TBody(
                //given
                classes = FakeClasses(),
                FakeTr()
            )
        )
    }

    @Test
    fun testSixthConstructorMixedParameters() {
        //then
        assertEquals(
            """<tbody fakeClasses><tr></tbody>""",
            //when
            TBody(
                //given
                classes = FakeClasses(),
                listOf(FakeTr())
            )
        )
    }
}