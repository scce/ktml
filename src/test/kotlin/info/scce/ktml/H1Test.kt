package info.scce.ktml

import info.scce.ktml.fake.FakeAttribute
import info.scce.ktml.fake.FakeClasses
import info.scce.ktml.fake.FakeInlineTag
import org.junit.jupiter.api.Test

class H1Test {

    @Test
    fun testSecondConstructor() {
        //then
        assertEquals(
            "<h1><tag></h1>",
            //when
            H1(
                //given
                FakeInlineTag()
            )
        )
    }

    @Test
    fun testSecondConstructorEmpty() {
        //then
        assertEquals(
            """<h1></h1>""",
            //when
            H1(
                //given
            )
        )
    }

    @Test
    fun testThirdConstructor() {
        //then
        assertEquals(
            """<h1 fakeAttribute></h1>""",
            //when
            H1(
                //given
                Attributes(FakeAttribute())
            )
        )
    }

    @Test
    fun testThirdConstructorNamedParameter() {
        //then
        assertEquals(
            """<h1 fakeAttribute></h1>""",
            //when
            H1(
                //given
                attributes = Attributes(FakeAttribute())
            )
        )
    }

    @Test
    fun testFourthConstructor() {
        //then
        assertEquals(
            """<h1 fakeClasses fakeAttribute></h1>""",
            //when
            H1(
                //given
                Attributes(FakeAttribute()),
                FakeClasses()
            )
        )
    }

    @Test
    fun testFourthConstructorNamedParameters() {
        //then
        assertEquals(
            """<h1 fakeClasses fakeAttribute></h1>""",
            //when
            H1(
                //given
                attributes = Attributes(FakeAttribute()),
                classes = FakeClasses()
            )
        )
    }

    @Test
    fun testFourthConstructorMixingParameters() {
        //then
        assertEquals(
            """<h1 fakeClasses fakeAttribute></h1>""",
            //when
            H1(
                //given
                attributes = Attributes(FakeAttribute()),
                FakeClasses()
            )
        )
    }

    @Test
    fun testFifthConstructor() {
        //then
        assertEquals(
            """<h1 fakeClasses><tag></h1>""",
            //when
            H1(
                //given
                FakeClasses(),
                FakeInlineTag(),
            )
        )
    }

    @Test
    fun testFifthConstructorMixedParameters() {
        //then
        assertEquals(
            """<h1 fakeClasses><tag></h1>""",
            //when
            H1(
                //given
                classes = FakeClasses(),
                FakeInlineTag()
            )
        )
    }

    @Test
    fun testSixthConstructorMixedParameters() {
        //then
        assertEquals(
            """<h1 fakeClasses><tag></h1>""",
            //when
            H1(
                //given
                classes = FakeClasses(),
                listOf(FakeInlineTag())
            )
        )
    }
}