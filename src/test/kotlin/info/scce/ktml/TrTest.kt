package info.scce.ktml

import info.scce.ktml.fake.FakeAttribute
import info.scce.ktml.fake.FakeClasses
import info.scce.ktml.fake.FakeTd
import org.junit.jupiter.api.Test

class TrTest {

    @Test
    fun testSecondConstructor() {
        //then
        assertEquals(
            "<tr><td></tr>",
            //when
            Tr(
                //given
                FakeTd()
            )
        )
    }

    @Test
    fun testSecondConstructorEmpty() {
        //then
        assertEquals(
            """<tr></tr>""",
            //when
            Tr(
                //given
            )
        )
    }

    @Test
    fun testThirdConstructor() {
        //then
        assertEquals(
            """<tr fakeAttribute></tr>""",
            //when
            Tr(
                //given
                Attributes(FakeAttribute())
            )
        )
    }

    @Test
    fun testThirdConstructorNamedParameter() {
        //then
        assertEquals(
            """<tr fakeAttribute></tr>""",
            //when
            Tr(
                //given
                attributes = Attributes(FakeAttribute())
            )
        )
    }

    @Test
    fun testFourthConstructor() {
        //then
        assertEquals(
            """<tr fakeClasses fakeAttribute></tr>""",
            //when
            Tr(
                //given
                Attributes(FakeAttribute()),
                FakeClasses()
            )
        )
    }

    @Test
    fun testFourthConstructorNamedParameters() {
        //then
        assertEquals(
            """<tr fakeClasses fakeAttribute></tr>""",
            //when
            Tr(
                //given
                attributes = Attributes(FakeAttribute()),
                classes = FakeClasses()
            )
        )
    }

    @Test
    fun testFourthConstructorMixingParameters() {
        //then
        assertEquals(
            """<tr fakeClasses fakeAttribute></tr>""",
            //when
            Tr(
                //given
                attributes = Attributes(FakeAttribute()),
                FakeClasses()
            )
        )
    }

    @Test
    fun testFifthConstructor() {
        //then
        assertEquals(
            """<tr fakeClasses><td></tr>""",
            //when
            Tr(
                //given
                FakeClasses(),
                FakeTd(),
            )
        )
    }

    @Test
    fun testFifthConstructorMixedParameters() {
        //then
        assertEquals(
            """<tr fakeClasses><td></tr>""",
            //when
            Tr(
                //given
                classes = FakeClasses(),
                FakeTd(),
            )
        )
    }

    @Test
    fun testSixthConstructorMixedParameters() {
        //then
        assertEquals(
            """<tr fakeClasses><td></tr>""",
            //when
            Tr(
                //given
                classes = FakeClasses(),
                listOf(FakeTd()),
            )
        )
    }


}