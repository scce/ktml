package info.scce.ktml

import info.scce.ktml.fake.FakeAttribute
import info.scce.ktml.fake.FakeClasses
import info.scce.ktml.fake.FakeTag
import org.junit.jupiter.api.Test

class NavTest {
    @Test
    fun testSecondConstructor() {
        //then
        assertEquals(
            "<nav><tag></nav>",
            //when
            Nav(
                //given
                FakeTag()
            )
        )
    }

    @Test
    fun testSecondConstructorEmpty() {
        //then
        assertEquals(
            """<nav></nav>""",
            //when
            Nav(
                //given
            )
        )
    }

    @Test
    fun testThirdConstructor() {
        //then
        assertEquals(
            """<nav fakeAttribute></nav>""",
            //when
            Nav(
                //given
                Attributes(FakeAttribute())
            )
        )
    }

    @Test
    fun testThirdConstructorNamedParameter() {
        //then
        assertEquals(
            """<nav fakeAttribute></nav>""",
            //when
            Nav(
                //given
                attributes = Attributes(FakeAttribute())
            )
        )
    }

    @Test
    fun testFourthConstructor() {
        //then
        assertEquals(
            """<nav fakeClasses fakeAttribute></nav>""",
            //when
            Nav(
                //given
                Attributes(FakeAttribute()),
                FakeClasses()
            )
        )
    }

    @Test
    fun testFourthConstructorNamedParameters() {
        //then
        assertEquals(
            """<nav fakeClasses fakeAttribute></nav>""",
            //when
            Nav(
                //given
                attributes = Attributes(FakeAttribute()),
                classes = FakeClasses()
            )
        )
    }

    @Test
    fun testFourthConstructorMixingParameters() {
        //then
        assertEquals(
            """<nav fakeClasses fakeAttribute></nav>""",
            //when
            Nav(
                //given
                attributes = Attributes(FakeAttribute()),
                FakeClasses()
            )
        )
    }

    @Test
    fun testFifthConstructor() {
        //then
        assertEquals(
            """<nav fakeClasses><tag></nav>""",
            //when
            Nav(
                //given
                FakeClasses(),
                FakeTag(),
            )
        )
    }

    @Test
    fun testFifthConstructorMixedParameters() {
        //then
        assertEquals(
            """<nav fakeClasses><tag></nav>""",
            //when
            Nav(
                //given
                classes = FakeClasses(),
                FakeTag()
            )
        )
    }

    @Test
    fun testSixthConstructorMixedParameters() {
        //then
        assertEquals(
            """<nav fakeClasses><tag></nav>""",
            //when
            Nav(
                //given
                classes = FakeClasses(),
                listOf(FakeTag())
            )
        )
    }

}