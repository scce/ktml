package info.scce.ktml

import info.scce.ktml.fake.FakeStringInterface
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class AttributeTest {
    @Test
    fun generated() {
        assertEquals(
            """class="red center"""",
            Attribute(
                "class",
                "red", "center"
            )
                .string()
        )
    }

    @Test
    fun withStringInterface() {
        assertEquals(
            """class="red"""",
            Attribute(
                "class",
                FakeStringInterface("red")
            )
                .string()
        )
    }

}