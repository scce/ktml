package info.scce.ktml

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class ClassesTest {

    @Test
    fun nested() {
        //given
        val classes = Classes(
            Classes("row"),
            "margin-top-2"
        )
        //then
        assertEquals(
            """class="row margin-top-2"""",
            // when
            classes.string()
        )
    }
}