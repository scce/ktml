package info.scce.ktml

import info.scce.ktml.fake.FakeAttribute
import info.scce.ktml.fake.FakeClasses
import info.scce.ktml.fake.FakeTag
import info.scce.ktml.fake.FakeTitle
import org.junit.jupiter.api.Test

class HeadTest {
    @Test
    fun testSecondConstructor() {
        //then
        assertEquals(
            "<head><title><tag></head>",
            //when
            Head(
                //given
                FakeTitle(),
                FakeTag(),
            )
        )
    }

    @Test
    fun testSecondConstructorEmpty() {
        //then
        assertEquals(
            """<head><title></head>""",
            //when
            Head(
                //given
                FakeTitle(),
            )
        )
    }

    @Test
    fun testThirdConstructor() {
        //then
        assertEquals(
            """<head fakeAttribute><title></head>""",
            //when
            Head(
                //given
                Attributes(FakeAttribute()),
                FakeTitle(),
            )
        )
    }

    @Test
    fun testThirdConstructorNamedParameter() {
        //then
        assertEquals(
            """<head fakeAttribute><title></head>""",
            //when
            Head(
                //given
                attributes = Attributes(FakeAttribute()),
                FakeTitle(),
            )
        )
    }

    @Test
    fun testFourthConstructor() {
        //then
        assertEquals(
            """<head fakeClasses fakeAttribute><title></head>""",
            //when
            Head(
                //given
                Attributes(FakeAttribute()),
                FakeClasses(),
                FakeTitle(),
            )
        )
    }

    @Test
    fun testFourthConstructorNamedParameters() {
        //then
        assertEquals(
            """<head fakeClasses fakeAttribute><title></head>""",
            //when
            Head(
                //given
                attributes = Attributes(FakeAttribute()),
                classes = FakeClasses(),
                FakeTitle(),
            )
        )
    }

    @Test
    fun testFourthConstructorMixingParameters() {
        //then
        assertEquals(
            """<head fakeClasses fakeAttribute><title></head>""",
            //when
            Head(
                //given
                attributes = Attributes(FakeAttribute()),
                FakeClasses(),
                FakeTitle(),
            )
        )
    }

    @Test
    fun testFifthConstructor() {
        //then
        assertEquals(
            """<head fakeClasses><title><tag></head>""",
            //when
            Head(
                //given
                FakeClasses(),
                FakeTitle(),
                FakeTag(),
            )
        )
    }

    @Test
    fun testFifthConstructorMixedParameters() {
        //then
        assertEquals(
            """<head fakeClasses><title><tag></head>""",
            //when
            Head(
                //given
                classes = FakeClasses(),
                FakeTitle(),
                FakeTag(),
            )
        )
    }

    @Test
    fun testSixthConstructorMixedParameters() {
        //then
        assertEquals(
            """<head fakeClasses><title><tag></head>""",
            //when
            Head(
                //given
                classes = FakeClasses(),
                FakeTitle(),
                listOf(FakeTag()),
            )
        )
    }
}