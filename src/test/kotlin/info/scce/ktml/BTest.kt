package info.scce.ktml

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class BTest {

    @Test
    fun test() {
        //then
        assertEquals(
            """<b>text</b>""",
            B(
                //given
                Text("text"),
                //when
            ).build().string()
        )
    }
}