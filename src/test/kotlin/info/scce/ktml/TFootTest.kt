package info.scce.ktml

import info.scce.ktml.fake.FakeAttribute
import info.scce.ktml.fake.FakeClasses
import info.scce.ktml.fake.FakeTr
import org.junit.jupiter.api.Test

class TFootTest {
    @Test
    fun testSecondConstructor() {
        //then
        assertEquals(
            "<tfoot><tr></tfoot>",
            //when
            TFoot(
                //given
                FakeTr()
            )
        )
    }

    @Test
    fun testSecondConstructorEmpty() {
        //then
        assertEquals(
            """<tfoot></tfoot>""",
            //when
            TFoot(
                //given
            )
        )
    }

    @Test
    fun testThirdConstructor() {
        //then
        assertEquals(
            """<tfoot fakeAttribute></tfoot>""",
            //when
            TFoot(
                //given
                Attributes(FakeAttribute())
            )
        )
    }

    @Test
    fun testThirdConstructorNamedParameter() {
        //then
        assertEquals(
            """<tfoot fakeAttribute></tfoot>""",
            //when
            TFoot(
                //given
                attributes = Attributes(FakeAttribute())
            )
        )
    }

    @Test
    fun testFourthConstructor() {
        //then
        assertEquals(
            """<tfoot fakeClasses fakeAttribute></tfoot>""",
            //when
            TFoot(
                //given
                Attributes(FakeAttribute()),
                FakeClasses()
            )
        )
    }

    @Test
    fun testFourthConstructorNamedParameters() {
        //then
        assertEquals(
            """<tfoot fakeClasses fakeAttribute></tfoot>""",
            //when
            TFoot(
                //given
                attributes = Attributes(FakeAttribute()),
                classes = FakeClasses()
            )
        )
    }

    @Test
    fun testFourthConstructorMixingParameters() {
        //then
        assertEquals(
            """<tfoot fakeClasses fakeAttribute></tfoot>""",
            //when
            TFoot(
                //given
                attributes = Attributes(FakeAttribute()),
                FakeClasses()
            )
        )
    }

    @Test
    fun testFifthConstructor() {
        //then
        assertEquals(
            """<tfoot fakeClasses><tr></tfoot>""",
            //when
            TFoot(
                //given
                FakeClasses(),
                FakeTr(),
            )
        )
    }

    @Test
    fun testFifthConstructorMixedParameters() {
        //then
        assertEquals(
            """<tfoot fakeClasses><tr></tfoot>""",
            //when
            TFoot(
                //given
                classes = FakeClasses(),
                FakeTr()
            )
        )
    }

    @Test
    fun testSixthConstructorMixedParameters() {
        //then
        assertEquals(
            """<tfoot fakeClasses><tr></tfoot>""",
            //when
            TFoot(
                //given
                classes = FakeClasses(),
                listOf(FakeTr())
            )
        )
    }
}