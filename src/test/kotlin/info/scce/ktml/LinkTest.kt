package info.scce.ktml

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class LinkTest {
    @Test
    fun test() {
        //then
        assertEquals(
            """<link rel="icon" href="/favicon.ico">""",
            Link(
                //given
                "icon",
                "/favicon.ico"
                //when
            ).build().string()
        )
    }
}