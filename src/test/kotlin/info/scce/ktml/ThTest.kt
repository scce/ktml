package info.scce.ktml

import info.scce.ktml.fake.FakeAttribute
import info.scce.ktml.fake.FakeClasses
import info.scce.ktml.fake.FakeColSpan
import info.scce.ktml.fake.FakeTag
import org.junit.jupiter.api.Test

class ThTest {

    @Test
    fun testSecondConstructor() {
        //then
        assertEquals(
            "<th><tag></th>",
            //when
            Th(
                //given
                FakeTag(),
            )
        )
    }

    @Test
    fun testThirdConstructor() {
        //then
        assertEquals(
            "<th fakeAttribute><tag></th>",
            //when
            Th(
                //given
                Attributes(FakeAttribute()),
                FakeTag(),
            )
        )
    }

    @Test
    fun testFourthConstructor() {
        //then
        assertEquals(
            "<th fakeClasses><tag></th>",
            //when
            Th(
                //given
                FakeClasses(),
                FakeTag(),
            )
        )
    }

    @Test
    fun testFifthConstructor() {
        //then
        assertEquals(
            "<th fakeColSpan><tag></th>",
            //when
            Th(
                //given
                FakeColSpan(),
                FakeTag(),
            )
        )
    }

    @Test
    fun testSixthConstructor() {
        //then
        assertEquals(
            "<th fakeClasses fakeAttribute><tag></th>",
            //when
            Th(
                //given
                Attributes(FakeAttribute()),
                FakeClasses(),
                FakeTag(),
            )
        )
    }

    @Test
    fun testSeventhConstructor() {
        //then
        assertEquals(
            "<th fakeColSpan fakeAttribute><tag></th>",
            //when
            Th(
                //given
                Attributes(FakeAttribute()),
                FakeColSpan(),
                FakeTag(),
            )
        )
    }

    @Test
    fun testEighthConstructor() {
        //then
        assertEquals(
            "<th fakeClasses fakeColSpan><tag></th>",
            //when
            Th(
                //given
                FakeClasses(),
                FakeColSpan(),
                FakeTag(),
            )
        )
    }

    @Test
    fun testNinthConstructor() {
        //then
        assertEquals(
            "<th fakeClasses fakeColSpan fakeAttribute><tag></th>",
            //when
            Th(
                //given
                Attributes(FakeAttribute()),
                FakeClasses(),
                FakeColSpan(),
                FakeTag(),
            )
        )
    }
}