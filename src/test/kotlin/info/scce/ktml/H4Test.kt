package info.scce.ktml

import info.scce.ktml.fake.FakeAttribute
import info.scce.ktml.fake.FakeClasses
import info.scce.ktml.fake.FakeInlineTag
import org.junit.jupiter.api.Test

class H4Test {

    @Test
    fun testSecondConstructor() {
        //then
        assertEquals(
            "<h4><tag></h4>",
            //when
            H4(
                //given
                FakeInlineTag()
            )
        )
    }

    @Test
    fun testSecondConstructorEmpty() {
        //then
        assertEquals(
            """<h4></h4>""",
            //when
            H4(
                //given
            )
        )
    }

    @Test
    fun testThirdConstructor() {
        //then
        assertEquals(
            """<h4 fakeAttribute></h4>""",
            //when
            H4(
                //given
                Attributes(FakeAttribute())
            )
        )
    }

    @Test
    fun testThirdConstructorNamedParameter() {
        //then
        assertEquals(
            """<h4 fakeAttribute></h4>""",
            //when
            H4(
                //given
                attributes = Attributes(FakeAttribute())
            )
        )
    }

    @Test
    fun testFourthConstructor() {
        //then
        assertEquals(
            """<h4 fakeClasses fakeAttribute></h4>""",
            //when
            H4(
                //given
                Attributes(FakeAttribute()),
                FakeClasses()
            )
        )
    }

    @Test
    fun testFourthConstructorNamedParameters() {
        //then
        assertEquals(
            """<h4 fakeClasses fakeAttribute></h4>""",
            //when
            H4(
                //given
                attributes = Attributes(FakeAttribute()),
                classes = FakeClasses()
            )
        )
    }

    @Test
    fun testFourthConstructorMixingParameters() {
        //then
        assertEquals(
            """<h4 fakeClasses fakeAttribute></h4>""",
            //when
            H4(
                //given
                attributes = Attributes(FakeAttribute()),
                FakeClasses()
            )
        )
    }

    @Test
    fun testFifthConstructor() {
        //then
        assertEquals(
            """<h4 fakeClasses><tag></h4>""",
            //when
            H4(
                //given
                FakeClasses(),
                FakeInlineTag(),
            )
        )
    }

    @Test
    fun testFifthConstructorMixedParameters() {
        //then
        assertEquals(
            """<h4 fakeClasses><tag></h4>""",
            //when
            H4(
                //given
                classes = FakeClasses(),
                FakeInlineTag()
            )
        )
    }

    @Test
    fun testSixthConstructorMixedParameters() {
        //then
        assertEquals(
            """<h4 fakeClasses><tag></h4>""",
            //when
            H4(
                //given
                classes = FakeClasses(),
                listOf(FakeInlineTag())
            )
        )
    }
}