package info.scce.ktml

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class MetaTest {
    @Test
    fun generated() {
        assertEquals(
            """<meta name="viewport" content="width=device-width, initial-scale=1">""",
            Meta(
                "viewport",
                "width=device-width, initial-scale=1",
            )
                .build().string()
        )
    }
}