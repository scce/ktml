package info.scce.ktml

import info.scce.ktml.fake.FakeAttribute
import info.scce.ktml.fake.FakeClasses
import info.scce.ktml.fake.FakeInlineTag
import org.junit.jupiter.api.Test

class H6Test {

    @Test
    fun testSecondConstructor() {
        //then
        assertEquals(
            "<h6><tag></h6>",
            //when
            H6(
                //given
                FakeInlineTag()
            )
        )
    }

    @Test
    fun testSecondConstructorEmpty() {
        //then
        assertEquals(
            """<h6></h6>""",
            //when
            H6(
                //given
            )
        )
    }

    @Test
    fun testThirdConstructor() {
        //then
        assertEquals(
            """<h6 fakeAttribute></h6>""",
            //when
            H6(
                //given
                Attributes(FakeAttribute())
            )
        )
    }

    @Test
    fun testThirdConstructorNamedParameter() {
        //then
        assertEquals(
            """<h6 fakeAttribute></h6>""",
            //when
            H6(
                //given
                attributes = Attributes(FakeAttribute())
            )
        )
    }

    @Test
    fun testFourthConstructor() {
        //then
        assertEquals(
            """<h6 fakeClasses fakeAttribute></h6>""",
            //when
            H6(
                //given
                Attributes(FakeAttribute()),
                FakeClasses()
            )
        )
    }

    @Test
    fun testFourthConstructorNamedParameters() {
        //then
        assertEquals(
            """<h6 fakeClasses fakeAttribute></h6>""",
            //when
            H6(
                //given
                attributes = Attributes(FakeAttribute()),
                classes = FakeClasses()
            )
        )
    }

    @Test
    fun testFourthConstructorMixingParameters() {
        //then
        assertEquals(
            """<h6 fakeClasses fakeAttribute></h6>""",
            //when
            H6(
                //given
                attributes = Attributes(FakeAttribute()),
                FakeClasses()
            )
        )
    }

    @Test
    fun testFifthConstructor() {
        //then
        assertEquals(
            """<h6 fakeClasses><tag></h6>""",
            //when
            H6(
                //given
                FakeClasses(),
                FakeInlineTag(),
            )
        )
    }

    @Test
    fun testFifthConstructorMixedParameters() {
        //then
        assertEquals(
            """<h6 fakeClasses><tag></h6>""",
            //when
            H6(
                //given
                classes = FakeClasses(),
                FakeInlineTag()
            )
        )
    }

    @Test
    fun testSixthConstructorMixedParameters() {
        //then
        assertEquals(
            """<h6 fakeClasses><tag></h6>""",
            //when
            H6(
                //given
                classes = FakeClasses(),
                listOf(FakeInlineTag())
            )
        )
    }
}