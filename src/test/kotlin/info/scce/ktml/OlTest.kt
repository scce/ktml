package info.scce.ktml

import info.scce.ktml.fake.FakeAttribute
import info.scce.ktml.fake.FakeClasses
import info.scce.ktml.fake.FakeLi
import org.junit.jupiter.api.Test

class OlTest {

    @Test
    fun testSecondConstructor() {
        //then
        assertEquals(
            "<ol><li></ol>",
            //when
            Ol(
                //given
                FakeLi()
            )
        )
    }

    @Test
    fun testSecondConstructorEmpty() {
        //then
        assertEquals(
            """<ol></ol>""",
            //when
            Ol(
                //given
            )
        )
    }

    @Test
    fun testThirdConstructor() {
        //then
        assertEquals(
            """<ol fakeAttribute></ol>""",
            //when
            Ol(
                //given
                Attributes(FakeAttribute())
            )
        )
    }

    @Test
    fun testThirdConstructorNamedParameter() {
        //then
        assertEquals(
            """<ol fakeAttribute></ol>""",
            //when
            Ol(
                //given
                attributes = Attributes(FakeAttribute())
            )
        )
    }

    @Test
    fun testFourthConstructor() {
        //then
        assertEquals(
            """<ol fakeClasses fakeAttribute></ol>""",
            //when
            Ol(
                //given
                Attributes(FakeAttribute()),
                FakeClasses()
            )
        )
    }

    @Test
    fun testFourthConstructorNamedParameters() {
        //then
        assertEquals(
            """<ol fakeClasses fakeAttribute></ol>""",
            //when
            Ol(
                //given
                attributes = Attributes(FakeAttribute()),
                classes = FakeClasses()
            )
        )
    }

    @Test
    fun testFourthConstructorMixingParameters() {
        //then
        assertEquals(
            """<ol fakeClasses fakeAttribute></ol>""",
            //when
            Ol(
                //given
                attributes = Attributes(FakeAttribute()),
                FakeClasses()
            )
        )
    }

    @Test
    fun testFifthConstructor() {
        //then
        assertEquals(
            """<ol fakeClasses><li></ol>""",
            //when
            Ol(
                //given
                FakeClasses(),
                FakeLi()
            )
        )
    }

    @Test
    fun testFifthConstructorMixedParameters() {
        //then
        assertEquals(
            """<ol fakeClasses><li></ol>""",
            //when
            Ol(
                //given
                classes = FakeClasses(),
                FakeLi()
            )
        )
    }

    @Test
    fun testSixthConstructorMixedParameters() {
        //then
        assertEquals(
            """<ol fakeClasses><li></ol>""",
            //when
            Ol(
                //given
                classes = FakeClasses(),
                listOf(FakeLi())
            )
        )
    }


}