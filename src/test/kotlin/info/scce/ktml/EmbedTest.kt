package info.scce.ktml

import info.scce.ktml.fake.FakeAttribute
import info.scce.ktml.fake.FakeClasses
import info.scce.ktml.fake.util.FakeSrc
import org.junit.jupiter.api.Test

class EmbedTest {

    @Test
    fun testSecondConstructor() {
        //then
        assertEquals(
            """<embed fakeSrc type="application/pdf">""",
            //when
            Embed(
                //given
                src = FakeSrc(),
                type = "application/pdf",
            )
        )
    }

    @Test
    fun testThirdConstructor() {
        //then
        assertEquals(
            """<embed fakeSrc type="application/pdf" fakeAttribute>""",
            //when
            Embed(
                //given
                src = FakeSrc(),
                type = "application/pdf",
                attributes = Attributes(FakeAttribute())
            )
        )
    }

    @Test
    fun testFourthConstructor() {
        //then
        assertEquals(
            """<embed fakeSrc type="application/pdf" fakeClasses>""",
            //when
            Embed(
                //given
                classes = FakeClasses(),
                src = FakeSrc(),
                type = "application/pdf",
            )
        )
    }
}