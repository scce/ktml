package info.scce.ktml

import info.scce.ktml.fake.FakeAttribute
import info.scce.ktml.fake.FakeClasses
import info.scce.ktml.fake.FakeInlineTag
import org.junit.jupiter.api.Test

class PTest {

    @Test
    fun testSecondConstructor() {
        //then
        assertEquals(
            "<p><tag></p>",
            //when
            P(
                //given
                FakeInlineTag()
            )
        )
    }

    @Test
    fun testSecondConstructorEmpty() {
        //then
        assertEquals(
            """<p></p>""",
            //when
            P(
                //given
            )
        )
    }

    @Test
    fun testThirdConstructor() {
        //then
        assertEquals(
            """<p fakeAttribute></p>""",
            //when
            P(
                //given
                Attributes(FakeAttribute())
            )
        )
    }

    @Test
    fun testThirdConstructorNamedParameter() {
        //then
        assertEquals(
            """<p fakeAttribute></p>""",
            //when
            P(
                //given
                attributes = Attributes(FakeAttribute())
            )
        )
    }

    @Test
    fun testFourthConstructor() {
        //then
        assertEquals(
            """<p fakeClasses fakeAttribute></p>""",
            //when
            P(
                //given
                Attributes(FakeAttribute()),
                FakeClasses()
            )
        )
    }

    @Test
    fun testFourthConstructorNamedParameters() {
        //then
        assertEquals(
            """<p fakeClasses fakeAttribute></p>""",
            //when
            P(
                //given
                attributes = Attributes(FakeAttribute()),
                classes = FakeClasses()
            )
        )
    }

    @Test
    fun testFourthConstructorMixingParameters() {
        //then
        assertEquals(
            """<p fakeClasses fakeAttribute></p>""",
            //when
            P(
                //given
                attributes = Attributes(FakeAttribute()),
                FakeClasses()
            )
        )
    }

    @Test
    fun testFifthConstructor() {
        //then
        assertEquals(
            """<p fakeClasses><tag></p>""",
            //when
            P(
                //given
                FakeClasses(),
                FakeInlineTag(),
            )
        )
    }

    @Test
    fun testFifthConstructorMixedParameters() {
        //then
        assertEquals(
            """<p fakeClasses><tag></p>""",
            //when
            P(
                //given
                classes = FakeClasses(),
                FakeInlineTag()
            )
        )
    }

    @Test
    fun testSixthConstructorMixedParameters() {
        //then
        assertEquals(
            """<p fakeClasses><tag></p>""",
            //when
            P(
                //given
                classes = FakeClasses(),
                listOf(FakeInlineTag())
            )
        )
    }
}