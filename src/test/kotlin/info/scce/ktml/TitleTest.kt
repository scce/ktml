package info.scce.ktml

import info.scce.ktml.fake.FakeText
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class TitleTest{
    @Test
    fun test() {
        //then
        assertEquals(
            "<title>text</title>",
            Title(
                //given
                FakeText()
                //when
            ).build().string()
        )
    }
}