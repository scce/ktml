package info.scce.ktml

import info.scce.ktml.fake.FakeAttribute
import info.scce.ktml.fake.FakeClasses
import info.scce.ktml.fake.FakeTag
import org.junit.jupiter.api.Test

class BodyTest {
    @Test
    fun testSecondConstructor() {
        //then
        assertEquals(
            "<body><tag></body>",
            //when
            Body(
                //given
                FakeTag()
            )
        )
    }

    @Test
    fun testSecondConstructorEmpty() {
        //then
        assertEquals(
            """<body></body>""",
            //when
            Body(
                //given
            )
        )
    }

    @Test
    fun testThirdConstructor() {
        //then
        assertEquals(
            """<body fakeAttribute></body>""",
            //when
            Body(
                //given
                Attributes(FakeAttribute())
            )
        )
    }

    @Test
    fun testThirdConstructorNamedParameter() {
        //then
        assertEquals(
            """<body fakeAttribute></body>""",
            //when
            Body(
                //given
                attributes = Attributes(FakeAttribute())
            )
        )
    }

    @Test
    fun testFourthConstructor() {
        //then
        assertEquals(
            """<body fakeClasses fakeAttribute></body>""",
            //when
            Body(
                //given
                Attributes(FakeAttribute()),
                FakeClasses()
            )
        )
    }

    @Test
    fun testFourthConstructorNamedParameters() {
        //then
        assertEquals(
            """<body fakeClasses fakeAttribute></body>""",
            //when
            Body(
                //given
                attributes = Attributes(FakeAttribute()),
                classes = FakeClasses()
            )
        )
    }

    @Test
    fun testFourthConstructorMixingParameters() {
        //then
        assertEquals(
            """<body fakeClasses fakeAttribute></body>""",
            //when
            Body(
                //given
                attributes = Attributes(FakeAttribute()),
                FakeClasses()
            )
        )
    }

    @Test
    fun testFifthConstructor() {
        //then
        assertEquals(
            """<body fakeClasses><tag></body>""",
            //when
            Body(
                //given
                FakeClasses(),
                FakeTag(),
            )
        )
    }

    @Test
    fun testFifthConstructorMixedParameters() {
        //then
        assertEquals(
            """<body fakeClasses><tag></body>""",
            //when
            Body(
                //given
                classes = FakeClasses(),
                FakeTag()
            )
        )
    }

    @Test
    fun testSixthConstructorMixedParameters() {
        //then
        assertEquals(
            """<body fakeClasses><tag></body>""",
            //when
            Body(
                //given
                classes = FakeClasses(),
                listOf(FakeTag())
            )
        )
    }
}