package info.scce.ktml

import info.scce.ktml.fake.FakeAttribute
import info.scce.ktml.fake.FakeClasses
import info.scce.ktml.fake.FakeInlineTag
import org.junit.jupiter.api.Test

class H3Test {

    @Test
    fun testSecondConstructor() {
        //then
        assertEquals(
            "<h3><tag></h3>",
            //when
            H3(
                //given
                FakeInlineTag()
            )
        )
    }

    @Test
    fun testSecondConstructorEmpty() {
        //then
        assertEquals(
            """<h3></h3>""",
            //when
            H3(
                //given
            )
        )
    }

    @Test
    fun testThirdConstructor() {
        //then
        assertEquals(
            """<h3 fakeAttribute></h3>""",
            //when
            H3(
                //given
                Attributes(FakeAttribute())
            )
        )
    }

    @Test
    fun testThirdConstructorNamedParameter() {
        //then
        assertEquals(
            """<h3 fakeAttribute></h3>""",
            //when
            H3(
                //given
                attributes = Attributes(FakeAttribute())
            )
        )
    }

    @Test
    fun testFourthConstructor() {
        //then
        assertEquals(
            """<h3 fakeClasses fakeAttribute></h3>""",
            //when
            H3(
                //given
                Attributes(FakeAttribute()),
                FakeClasses()
            )
        )
    }

    @Test
    fun testFourthConstructorNamedParameters() {
        //then
        assertEquals(
            """<h3 fakeClasses fakeAttribute></h3>""",
            //when
            H3(
                //given
                attributes = Attributes(FakeAttribute()),
                classes = FakeClasses()
            )
        )
    }

    @Test
    fun testFourthConstructorMixingParameters() {
        //then
        assertEquals(
            """<h3 fakeClasses fakeAttribute></h3>""",
            //when
            H3(
                //given
                attributes = Attributes(FakeAttribute()),
                FakeClasses()
            )
        )
    }

    @Test
    fun testFifthConstructor() {
        //then
        assertEquals(
            """<h3 fakeClasses><tag></h3>""",
            //when
            H3(
                //given
                FakeClasses(),
                FakeInlineTag(),
            )
        )
    }

    @Test
    fun testFifthConstructorMixedParameters() {
        //then
        assertEquals(
            """<h3 fakeClasses><tag></h3>""",
            //when
            H3(
                //given
                classes = FakeClasses(),
                FakeInlineTag()
            )
        )
    }

    @Test
    fun testSixthConstructorMixedParameters() {
        //then
        assertEquals(
            """<h3 fakeClasses><tag></h3>""",
            //when
            H3(
                //given
                classes = FakeClasses(),
                listOf(FakeInlineTag())
            )
        )
    }
}