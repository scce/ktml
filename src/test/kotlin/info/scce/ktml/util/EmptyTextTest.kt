package info.scce.ktml.util

import info.scce.ktml.assertEquals
import org.junit.jupiter.api.Test

class EmptyTextTest {

    @Test
    fun string() {
        //then
        assertEquals(
            "",
            EmptyText()
        )
    }
}