package info.scce.ktml.util

import org.junit.jupiter.api.Assertions.fail
import org.junit.jupiter.api.Test

class EmptyClassTest {

    @Test
    fun test() {
        try {
            EmptyClasses().string()
            fail("Expected NotImplementedError to be thrown")
        } catch (_: NotImplementedError) {

        }
    }
}