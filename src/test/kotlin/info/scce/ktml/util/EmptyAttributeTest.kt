package info.scce.ktml.util

import org.junit.jupiter.api.Assertions.fail
import org.junit.jupiter.api.Test

class EmptyAttributeTest {

    @Test
    fun test() {
        try {
            EmptyAttribute().string()
            fail("Expected NotImplementedError to be thrown")
        } catch (_: NotImplementedError) {

        }
    }
}