package info.scce.ktml.util

import info.scce.ktml.fake.FakeStringInterface
import info.scce.ktml.util.Href
import info.scce.ktml.util.Url
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class HrefTest {

    @Test
    fun attribute() {
        //then
        assertEquals(
            """href="/"""",
            Href(
                //given
                Url()
                //when
            ).string()
        )
    }

    @Test
    fun stringInterface() {
        //then
        assertEquals(
            """href="/"""",
            Href(
                //given
                FakeStringInterface("/")
                //when
            ).string()
        )
    }

}