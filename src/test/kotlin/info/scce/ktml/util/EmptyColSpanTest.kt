package info.scce.ktml.util

import org.junit.jupiter.api.Assertions.fail
import org.junit.jupiter.api.Test

class EmptyColSpanTest {

    @Test
    fun test() {
        try {
            EmptyColSpan().string()
            fail("Expected NotImplementedError to be thrown")
        } catch (_: NotImplementedError) {

        }
    }
}