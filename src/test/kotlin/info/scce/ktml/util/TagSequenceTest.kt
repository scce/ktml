package info.scce.ktml.util

import info.scce.ktml.fake.FakeTag
import info.scce.ktml.util.TagSequence
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class TagSequenceTest {
    @Test
    fun test() {
        assertEquals(
            "<tag>",
            TagSequence(
                FakeTag()
            ).string()
        )
    }
}