package info.scce.ktml.util

import info.scce.ktml.MultilineText
import info.scce.ktml.Text
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test


class MultilineTextTest {
    @Test
    fun test() {
        //then
        assertEquals(
            "first line<br>second line",
            MultilineText(
                //given
                "first line",
                "second line"
            )
                //when
                .string()
        )
    }

    @Test
    fun withTexts() {
        //then
        assertEquals(
            "first<br>line",
            MultilineText(
                //given
                Text("first"),
                Text("line")
            )
                //when
                .string()
        )
    }
}