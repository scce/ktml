package info.scce.ktml.util

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class FakeSrcTest {
    @Test
    fun test() {
        //then
        assertEquals(
            """src="/path"""",
            Src(
                //given
                Url("path")
                //when
            ).string()
        )
    }
}