package info.scce.ktml.util

import info.scce.ktml.util.StylesheetLink
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class StylesheetLinkTest{
    @Test
    fun name() {
        //then
        assertEquals(
            """<link rel="stylesheet" href="/style.css">""",
            StylesheetLink(
                //given
                "/style.css"
                //when
            ).build().string()
        )
    }
}