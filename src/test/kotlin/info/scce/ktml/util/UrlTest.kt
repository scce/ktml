package info.scce.ktml.util

import info.scce.ktml.util.Url
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class UrlTest {
    @Test
    fun singlePath() {
        //then
        assertEquals(
            "/base",
            //when
            Url(
                //given
                "base",
            ).string()
        )
    }

    @Test
    fun multiplePathSegments() {
        //then
        assertEquals(
            "/base/suffix",
            //when
            Url(
                //given
                "base",
                "suffix"
            ).string()
        )
    }

    @Test
    fun withIntSuffix() {
        //then
        assertEquals(
            "/base/1",
            //when
            Url(
                //given
                Url("base"),
                1
            ).string()
        )
    }

    @Test
    fun withStringSuffix() {
        //then
        assertEquals(
            "/base/second",
            //when
            Url(
                //given
                Url("base"),
                "second"
            ).string()
        )
    }

    @Test
    fun withIntSuffixAndAnchor() {
        //then
        assertEquals(
            "/base/1#anchor",
            //when
            Url(
                //given
                Url("base"),
                1,
                anchor="anchor"
            ).string()
        )
    }

    @Test
    fun withStringSuffixAndAnchor() {
        //then
        assertEquals(
            "/base/second#anchor",
            //when
            Url(
                //given
                Url("base"),
                "second",
                anchor="anchor"
            ).string()
        )
    }

}