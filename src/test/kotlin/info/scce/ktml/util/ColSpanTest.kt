package info.scce.ktml.util

import info.scce.ktml.assertEquals
import org.junit.jupiter.api.Test

class ColSpanTest {


    @Test
    fun attribute() {
        //then
        assertEquals(
            """colspan="2"""",
            ColSpan(
                //given
                2
                //when
            )
        )
    }


}