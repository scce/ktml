package info.scce.ktml.util

import info.scce.ktml.util.Favicon
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class FaviconTest{
    @Test
    fun name() {
        //then
        assertEquals(
            """<link rel="icon" href="/favicon.png">""",
            Favicon(
                //given
                "/favicon.png"
                //when
            ).build().string()
        )
    }
}