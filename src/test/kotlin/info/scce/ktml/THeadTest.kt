package info.scce.ktml

import info.scce.ktml.fake.FakeAttribute
import info.scce.ktml.fake.FakeClasses
import info.scce.ktml.fake.FakeTr
import org.junit.jupiter.api.Test

class THeadTest {
    @Test
    fun testSecondConstructor() {
        //then
        assertEquals(
            "<thead><tr></thead>",
            //when
            THead(
                //given
                FakeTr()
            )
        )
    }

    @Test
    fun testSecondConstructorEmpty() {
        //then
        assertEquals(
            """<thead></thead>""",
            //when
            THead(
                //given
            )
        )
    }

    @Test
    fun testThirdConstructor() {
        //then
        assertEquals(
            """<thead fakeAttribute></thead>""",
            //when
            THead(
                //given
                Attributes(FakeAttribute())
            )
        )
    }

    @Test
    fun testThirdConstructorNamedParameter() {
        //then
        assertEquals(
            """<thead fakeAttribute></thead>""",
            //when
            THead(
                //given
                attributes = Attributes(FakeAttribute())
            )
        )
    }

    @Test
    fun testFourthConstructor() {
        //then
        assertEquals(
            """<thead fakeClasses fakeAttribute></thead>""",
            //when
            THead(
                //given
                Attributes(FakeAttribute()),
                FakeClasses()
            )
        )
    }

    @Test
    fun testFourthConstructorNamedParameters() {
        //then
        assertEquals(
            """<thead fakeClasses fakeAttribute></thead>""",
            //when
            THead(
                //given
                attributes = Attributes(FakeAttribute()),
                classes = FakeClasses()
            )
        )
    }

    @Test
    fun testFourthConstructorMixingParameters() {
        //then
        assertEquals(
            """<thead fakeClasses fakeAttribute></thead>""",
            //when
            THead(
                //given
                attributes = Attributes(FakeAttribute()),
                FakeClasses()
            )
        )
    }

    @Test
    fun testFifthConstructor() {
        //then
        assertEquals(
            """<thead fakeClasses><tr></thead>""",
            //when
            THead(
                //given
                FakeClasses(),
                FakeTr(),
            )
        )
    }

    @Test
    fun testFifthConstructorMixedParameters() {
        //then
        assertEquals(
            """<thead fakeClasses><tr></thead>""",
            //when
            THead(
                //given
                classes = FakeClasses(),
                FakeTr()
            )
        )
    }

    @Test
    fun testSixthConstructorMixedParameters() {
        //then
        assertEquals(
            """<thead fakeClasses><tr></thead>""",
            //when
            THead(
                //given
                classes = FakeClasses(),
                listOf(FakeTr())
            )
        )
    }
}