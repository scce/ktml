package info.scce.ktml

import info.scce.ktml.fake.FakeAttribute
import info.scce.ktml.fake.FakeClasses
import info.scce.ktml.fake.FakeColSpan
import info.scce.ktml.fake.FakeTag
import org.junit.jupiter.api.Test

class TdTest {

    @Test
    fun testSecondConstructor() {
        //then
        assertEquals(
            "<td><tag></td>",
            //when
            Td(
                //given
                FakeTag(),
            )
        )
    }

    @Test
    fun testThirdConstructor() {
        //then
        assertEquals(
            "<td fakeAttribute><tag></td>",
            //when
            Td(
                //given
                Attributes(FakeAttribute()),
                FakeTag(),
            )
        )
    }

    @Test
    fun testFourthConstructor() {
        //then
        assertEquals(
            "<td fakeClasses><tag></td>",
            //when
            Td(
                //given
                FakeClasses(),
                FakeTag(),
            )
        )
    }

    @Test
    fun testFifthConstructor() {
        //then
        assertEquals(
            "<td fakeColSpan><tag></td>",
            //when
            Td(
                //given
                FakeColSpan(),
                FakeTag(),
            )
        )
    }

    @Test
    fun testSixthConstructor() {
        //then
        assertEquals(
            "<td fakeClasses fakeAttribute><tag></td>",
            //when
            Td(
                //given
                Attributes(FakeAttribute()),
                FakeClasses(),
                FakeTag(),
            )
        )
    }

    @Test
    fun testSeventhConstructor() {
        //then
        assertEquals(
            "<td fakeColSpan fakeAttribute><tag></td>",
            //when
            Td(
                //given
                Attributes(FakeAttribute()),
                FakeColSpan(),
                FakeTag(),
            )
        )
    }

    @Test
    fun testEighthConstructor() {
        //then
        assertEquals(
            "<td fakeClasses fakeColSpan><tag></td>",
            //when
            Td(
                //given
                FakeClasses(),
                FakeColSpan(),
                FakeTag(),
            )
        )
    }

    @Test
    fun testNinthConstructor() {
        //then
        assertEquals(
            "<td fakeClasses fakeColSpan fakeAttribute><tag></td>",
            //when
            Td(
                //given
                Attributes(FakeAttribute()),
                FakeClasses(),
                FakeColSpan(),
                FakeTag(),
            )
        )
    }


}