package info.scce.ktml

import info.scce.ktml.fake.*
import org.junit.jupiter.api.Test

class TableTest {

    @Test
    fun testSecondConstructor() {
        //then
        assertEquals(
            "<table fakeAttribute><thead></table>",
            //when
            Table(
                //given
                Attributes(FakeAttribute()),
                FakeTHead(),
            )
        )
    }

    @Test
    fun testThirdConstructor() {
        //then
        assertEquals(
            "<table fakeAttribute><tbody></table>",
            //when
            Table(
                //given
                Attributes(FakeAttribute()),
                FakeTBody(),
            )
        )
    }

    @Test
    fun testFourthConstructor() {
        //then
        assertEquals(
            "<table fakeAttribute><tfoot></table>",
            //when
            Table(
                //given
                Attributes(FakeAttribute()),
                FakeTFoot(),
            )
        )
    }

    @Test
    fun testFifthConstructor() {
        //then
        assertEquals(
            "<table fakeClasses><thead></table>",
            //when
            Table(
                //given
                FakeClasses(),
                FakeTHead(),
            )
        )
    }

    @Test
    fun testSixthConstructor() {
        //then
        assertEquals(
            "<table fakeClasses><tbody></table>",
            //when
            Table(
                //given
                FakeClasses(),
                FakeTBody(),
            )
        )
    }

    @Test
    fun testSeventhConstructor() {
        //then
        assertEquals(
            "<table fakeClasses><tfoot></table>",
            //when
            Table(
                //given
                FakeClasses(),
                FakeTFoot(),
            )
        )
    }

    @Test
    fun testEighthConstructor() {
        //then
        assertEquals(
            "<table fakeClasses fakeAttribute><thead></table>",
            //when
            Table(
                //given
                Attributes(FakeAttribute()),
                FakeClasses(),
                FakeTHead(),
            )
        )
    }

    @Test
    fun testNinthConstructor() {
        //then
        assertEquals(
            "<table fakeClasses fakeAttribute><tbody></table>",
            //when
            Table(
                //given
                Attributes(FakeAttribute()),
                FakeClasses(),
                FakeTBody(),
            )
        )
    }

    @Test
    fun testTenthConstructor() {
        //then
        assertEquals(
            "<table fakeClasses fakeAttribute><tfoot></table>",
            //when
            Table(
                //given
                Attributes(FakeAttribute()),
                FakeClasses(),
                FakeTFoot(),
            )
        )
    }

    @Test
    fun testEleventhConstructor() {
        //then
        assertEquals(
            "<table fakeAttribute><thead><tbody></table>",
            //when
            Table(
                //given
                Attributes(FakeAttribute()),
                FakeTHead(),
                FakeTBody(),
            )
        )
    }

    @Test
    fun testTwelveConstructor() {
        //then
        assertEquals(
            "<table fakeAttribute><thead><tfoot></table>",
            //when
            Table(
                //given
                Attributes(FakeAttribute()),
                FakeTHead(),
                FakeTFoot(),
            )
        )
    }

    @Test
    fun testThirteenthConstructor() {
        //then
        assertEquals(
            "<table fakeAttribute><tbody><tfoot></table>",
            //when
            Table(
                //given
                Attributes(FakeAttribute()),
                FakeTBody(),
                FakeTFoot(),
            )
        )
    }

    @Test
    fun testFourteenthConstructor() {
        //then
        assertEquals(
            "<table fakeClasses><thead><tbody></table>",
            //when
            Table(
                //given
                FakeClasses(),
                FakeTHead(),
                FakeTBody(),
            )
        )
    }

    @Test
    fun testFifteenthConstructor() {
        //then
        assertEquals(
            "<table fakeClasses><thead><tfoot></table>",
            //when
            Table(
                //given
                FakeClasses(),
                FakeTHead(),
                FakeTFoot(),
            )
        )
    }

    @Test
    fun testSixteenthConstructor() {
        //then
        assertEquals(
            "<table fakeClasses><tbody><tfoot></table>",
            //when
            Table(
                //given
                FakeClasses(),
                FakeTBody(),
                FakeTFoot(),
            )
        )
    }

    @Test
    fun testSeventeenthConstructor() {
        //then
        assertEquals(
            "<table fakeAttribute><thead><tbody><tfoot></table>",
            //when
            Table(
                //given
                Attributes(FakeAttribute()),
                FakeTHead(),
                FakeTBody(),
                FakeTFoot(),
            )
        )
    }

    @Test
    fun testEighteenthConstructor() {
        //then
        assertEquals(
            "<table fakeClasses><thead><tbody><tfoot></table>",
            //when
            Table(
                //given
                FakeClasses(),
                FakeTHead(),
                FakeTBody(),
                FakeTFoot(),
            )
        )
    }
}