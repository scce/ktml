package info.scce.ktml

import info.scce.ktml.fake.FakeAttribute
import info.scce.ktml.fake.FakeClasses
import org.junit.jupiter.api.Test

class HrTest {
    @Test
    fun testFirstConstructor() {
        //then
        assertEquals(
            "<hr fakeClasses fakeAttribute>",
            //when
            Hr(
                //given
                Attributes(FakeAttribute()),
                FakeClasses()
            )
        )
    }

    @Test
    fun testSecondConstructor() {
        //then
        assertEquals(
            "<hr>",
            //when
            Hr(
                //given
            )
        )
    }

    @Test
    fun testThirdConstructor() {
        //then
        assertEquals(
            "<hr fakeClasses>",
            //when
            Hr(
                //given
                FakeClasses()
            )
        )
    }

    @Test
    fun testFourthConstructor() {
        //then
        assertEquals(
            "<hr fakeAttribute>",
            //when
            Hr(
                //given
                Attributes(FakeAttribute())
            )
        )
    }
}