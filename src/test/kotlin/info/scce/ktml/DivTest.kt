package info.scce.ktml

import info.scce.ktml.fake.FakeAttribute
import info.scce.ktml.fake.FakeClasses
import info.scce.ktml.fake.FakeTag
import info.scce.ktml.util.Href
import org.junit.jupiter.api.Test


class DivTest {
    @Test
    fun testSecondConstructor() {
        //then
        assertEquals(
            "<div><tag></div>",
            //when
            Div(
                //given
                FakeTag()
            )
        )
    }

    @Test
    fun testSecondConstructorEmpty() {
        //then
        assertEquals(
            """<div></div>""",
            //when
            Div(
                //given
            )
        )
    }

    @Test
    fun testThirdConstructor() {
        //then
        assertEquals(
            """<div fakeAttribute></div>""",
            //when
            Div(
                //given
                Attributes(FakeAttribute())
            )
        )
    }

    @Test
    fun testThirdConstructorNamedParameter() {
        //then
        assertEquals(
            """<div fakeAttribute></div>""",
            //when
            Div(
                //given
                attributes = Attributes(FakeAttribute())
            )
        )
    }

    @Test
    fun testFourthConstructor() {
        //then
        assertEquals(
            """<div fakeClasses fakeAttribute></div>""",
            //when
            Div(
                //given
                Attributes(FakeAttribute()),
                FakeClasses()
            )
        )
    }

    @Test
    fun testFourthConstructorNamedParameters() {
        //then
        assertEquals(
            """<div fakeClasses fakeAttribute></div>""",
            //when
            Div(
                //given
                attributes = Attributes(FakeAttribute()),
                classes = FakeClasses()
            )
        )
    }

    @Test
    fun testFourthConstructorMixingParameters() {
        //then
        assertEquals(
            """<div fakeClasses fakeAttribute></div>""",
            //when
            Div(
                //given
                attributes = Attributes(FakeAttribute()),
                FakeClasses()
            )
        )
    }

    @Test
    fun testFifthConstructor() {
        //then
        assertEquals(
            """<div fakeClasses><tag></div>""",
            //when
            Div(
                //given
                FakeClasses(),
                FakeTag(),
            )
        )
    }

    @Test
    fun testFifthConstructorMixedParameters() {
        //then
        assertEquals(
            """<div fakeClasses><tag></div>""",
            //when
            Div(
                //given
                classes = FakeClasses(),
                FakeTag()
            )
        )
    }

    @Test
    fun testSixthConstructorMixedParameters() {
        //then
        assertEquals(
            """<div fakeClasses><tag></div>""",
            //when
            Div(
                //given
                classes = FakeClasses(),
                listOf(FakeTag())
            )
        )
    }

    // todo update example in README
    @Test
    fun jumbotron() {
        //then
        assertEquals(
            """<div class="jumbotron"><h1 class="display-4">Hello, world!</h1><p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p><hr class="my-4"><p>It uses utility classes for typography and spacing to space content out within the larger container.</p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a></div>""",
            //when
            Div(
                //given
                classes = Classes("jumbotron"),
                H1(
                    classes = Classes("display-4"),
                    Text("Hello, world!"),
                ),
                P(
                    classes = Classes("lead"),
                    Text("This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information."),
                ),
                Hr(
                    classes = Classes("my-4")
                ),
                P(
                    Text("It uses utility classes for typography and spacing to space content out within the larger container.")
                ),
                A(
                    attributes = Attributes(Attribute("role", "button")),
                    classes = Classes("btn", "btn-primary", "btn-lg"),
                    href = Href("#"),
                    Text("Learn more"),
                ),
            )
        )
    }
}