package info.scce.ktml

import info.scce.ktml.fake.FakeAttribute
import info.scce.ktml.fake.FakeClasses
import info.scce.ktml.fake.FakeTag
import org.junit.jupiter.api.Test

class LiTest {
    @Test
    fun testSecondConstructor() {
        //then
        assertEquals(
            "<li><tag></li>",
            //when
            Li(
                //given
                FakeTag()
            )
        )
    }

    @Test
    fun testSecondConstructorEmpty() {
        //then
        assertEquals(
            """<li></li>""",
            //when
            Li(
                //given
            )
        )
    }

    @Test
    fun testThirdConstructor() {
        //then
        assertEquals(
            """<li fakeAttribute></li>""",
            //when
            Li(
                //given
                Attributes(FakeAttribute())
            )
        )
    }

    @Test
    fun testThirdConstructorNamedParameter() {
        //then
        assertEquals(
            """<li fakeAttribute></li>""",
            //when
            Li(
                //given
                attributes = Attributes(FakeAttribute())
            )
        )
    }

    @Test
    fun testFourthConstructor() {
        //then
        assertEquals(
            """<li fakeClasses fakeAttribute></li>""",
            //when
            Li(
                //given
                Attributes(FakeAttribute()),
                FakeClasses()
            )
        )
    }

    @Test
    fun testFourthConstructorNamedParameters() {
        //then
        assertEquals(
            """<li fakeClasses fakeAttribute></li>""",
            //when
            Li(
                //given
                attributes = Attributes(FakeAttribute()),
                classes = FakeClasses()
            )
        )
    }

    @Test
    fun testFourthConstructorMixingParameters() {
        //then
        assertEquals(
            """<li fakeClasses fakeAttribute></li>""",
            //when
            Li(
                //given
                attributes = Attributes(FakeAttribute()),
                FakeClasses()
            )
        )
    }

    @Test
    fun testFifthConstructor() {
        //then
        assertEquals(
            """<li fakeClasses><tag></li>""",
            //when
            Li(
                //given
                FakeClasses(),
                FakeTag(),
            )
        )
    }

    @Test
    fun testFifthConstructorMixedParameters() {
        //then
        assertEquals(
            """<li fakeClasses><tag></li>""",
            //when
            Li(
                //given
                classes = FakeClasses(),
                FakeTag()
            )
        )
    }

    @Test
    fun testSixthConstructorMixedParameters() {
        //then
        assertEquals(
            """<li fakeClasses><tag></li>""",
            //when
            Li(
                //given
                classes = FakeClasses(),
                listOf(FakeTag())
            )
        )
    }
}