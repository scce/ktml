package info.scce.ktml

import info.scce.ktml.fake.FakeAttribute
import info.scce.ktml.fake.FakeClasses
import info.scce.ktml.fake.FakeInlineTag
import org.junit.jupiter.api.Test

class H2Test {

    @Test
    fun testSecondConstructor() {
        //then
        assertEquals(
            "<h2><tag></h2>",
            //when
            H2(
                //given
                FakeInlineTag()
            )
        )
    }

    @Test
    fun testSecondConstructorEmpty() {
        //then
        assertEquals(
            """<h2></h2>""",
            //when
            H2(
                //given
            )
        )
    }

    @Test
    fun testThirdConstructor() {
        //then
        assertEquals(
            """<h2 fakeAttribute></h2>""",
            //when
            H2(
                //given
                Attributes(FakeAttribute())
            )
        )
    }

    @Test
    fun testThirdConstructorNamedParameter() {
        //then
        assertEquals(
            """<h2 fakeAttribute></h2>""",
            //when
            H2(
                //given
                attributes = Attributes(FakeAttribute())
            )
        )
    }

    @Test
    fun testFourthConstructor() {
        //then
        assertEquals(
            """<h2 fakeClasses fakeAttribute></h2>""",
            //when
            H2(
                //given
                Attributes(FakeAttribute()),
                FakeClasses()
            )
        )
    }

    @Test
    fun testFourthConstructorNamedParameters() {
        //then
        assertEquals(
            """<h2 fakeClasses fakeAttribute></h2>""",
            //when
            H2(
                //given
                attributes = Attributes(FakeAttribute()),
                classes = FakeClasses()
            )
        )
    }

    @Test
    fun testFourthConstructorMixingParameters() {
        //then
        assertEquals(
            """<h2 fakeClasses fakeAttribute></h2>""",
            //when
            H2(
                //given
                attributes = Attributes(FakeAttribute()),
                FakeClasses()
            )
        )
    }

    @Test
    fun testFifthConstructor() {
        //then
        assertEquals(
            """<h2 fakeClasses><tag></h2>""",
            //when
            H2(
                //given
                FakeClasses(),
                FakeInlineTag(),
            )
        )
    }

    @Test
    fun testFifthConstructorMixedParameters() {
        //then
        assertEquals(
            """<h2 fakeClasses><tag></h2>""",
            //when
            H2(
                //given
                classes = FakeClasses(),
                FakeInlineTag()
            )
        )
    }

    @Test
    fun testSixthConstructorMixedParameters() {
        //then
        assertEquals(
            """<h2 fakeClasses><tag></h2>""",
            //when
            H2(
                //given
                classes = FakeClasses(),
                listOf(FakeInlineTag())
            )
        )
    }
}