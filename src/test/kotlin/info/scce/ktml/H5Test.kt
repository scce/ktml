package info.scce.ktml

import info.scce.ktml.fake.FakeAttribute
import info.scce.ktml.fake.FakeClasses
import info.scce.ktml.fake.FakeInlineTag
import org.junit.jupiter.api.Test

class H5Test {

    @Test
    fun testSecondConstructor() {
        //then
        assertEquals(
            "<h5><tag></h5>",
            //when
            H5(
                //given
                FakeInlineTag()
            )
        )
    }

    @Test
    fun testSecondConstructorEmpty() {
        //then
        assertEquals(
            """<h5></h5>""",
            //when
            H5(
                //given
            )
        )
    }

    @Test
    fun testThirdConstructor() {
        //then
        assertEquals(
            """<h5 fakeAttribute></h5>""",
            //when
            H5(
                //given
                Attributes(FakeAttribute())
            )
        )
    }

    @Test
    fun testThirdConstructorNamedParameter() {
        //then
        assertEquals(
            """<h5 fakeAttribute></h5>""",
            //when
            H5(
                //given
                attributes = Attributes(FakeAttribute())
            )
        )
    }

    @Test
    fun testFourthConstructor() {
        //then
        assertEquals(
            """<h5 fakeClasses fakeAttribute></h5>""",
            //when
            H5(
                //given
                Attributes(FakeAttribute()),
                FakeClasses()
            )
        )
    }

    @Test
    fun testFourthConstructorNamedParameters() {
        //then
        assertEquals(
            """<h5 fakeClasses fakeAttribute></h5>""",
            //when
            H5(
                //given
                attributes = Attributes(FakeAttribute()),
                classes = FakeClasses()
            )
        )
    }

    @Test
    fun testFourthConstructorMixingParameters() {
        //then
        assertEquals(
            """<h5 fakeClasses fakeAttribute></h5>""",
            //when
            H5(
                //given
                attributes = Attributes(FakeAttribute()),
                FakeClasses()
            )
        )
    }

    @Test
    fun testFifthConstructor() {
        //then
        assertEquals(
            """<h5 fakeClasses><tag></h5>""",
            //when
            H5(
                //given
                FakeClasses(),
                FakeInlineTag(),
            )
        )
    }

    @Test
    fun testFifthConstructorMixedParameters() {
        //then
        assertEquals(
            """<h5 fakeClasses><tag></h5>""",
            //when
            H5(
                //given
                classes = FakeClasses(),
                FakeInlineTag()
            )
        )
    }

    @Test
    fun testSixthConstructorMixedParameters() {
        //then
        assertEquals(
            """<h5 fakeClasses><tag></h5>""",
            //when
            H5(
                //given
                classes = FakeClasses(),
                listOf(FakeInlineTag())
            )
        )
    }
}