package info.scce.ktml

import info.scce.ktml.fake.FakeAttribute
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class AttributesTest {

    @Test
    fun generated() {
        //then
        assertEquals(
            "",
            Attributes(
                //given
                // empty attributes
                //when
            ).string()
        )
    }

    @Test
    fun generatedSingleAttribute() {
        //then
        assertEquals(
            " fakeAttribute",
            Attributes(
                //given
                FakeAttribute()
                //when
            ).string()
        )
    }

    @Test
    fun generatedDoubleAttribute() {
        //then
        assertEquals(
            " attribute1 attribute2",
            Attributes(
                //given
                FakeAttribute("attribute1"),
                FakeAttribute("attribute2")
                //when
            ).string()
        )
    }
}