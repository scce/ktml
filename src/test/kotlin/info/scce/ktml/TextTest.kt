package info.scce.ktml

import info.scce.ktml.fake.FakeStringInterface
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test


class TextTest {
    @Test
    fun test() {
        //then
        assertEquals(
            "first linesecond line",
            Text(
                //given
                "first line",
                "second line"
            )
                //when
                .string()
        )
    }

    @Test
    fun withTexts() {
        //then
        assertEquals(
            "firstline",
            Text(
                //given
                Text("first"),
                Text("line")
            )
                //when
                .string()
        )
    }

    @Test
    fun withStringInterface() {
        //then
        assertEquals(
            "first",
            Text(
                //given
                FakeStringInterface("first")
            )
                //when
                .string()
        )
    }
}