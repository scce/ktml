package info.scce.ktml

import info.scce.ktml.interfaces.TagInterface
import info.scce.ktml.interfaces.TextInterface
import info.scce.ktml.util.AbstractAttributeDecorator
import info.scce.ktml.util.AbstractTagBuilder

fun assertEquals(
    expected: String,
    actual: AbstractTagBuilder<TagInterface>,
) {
    org.junit.jupiter.api.Assertions.assertEquals(
        expected,
        actual.build().string()
    )
}

fun assertEquals(
    expected: String,
    actual: AbstractAttributeDecorator,
) {
    org.junit.jupiter.api.Assertions.assertEquals(
        expected,
        actual.string()
    )
}

fun assertEquals(
    expected: String,
    actual: TextInterface,
) {
    org.junit.jupiter.api.Assertions.assertEquals(
        expected,
        actual.string()
    )
}
