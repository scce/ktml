package info.scce.ktml

import info.scce.ktml.fake.FakeAttribute
import info.scce.ktml.fake.FakeClasses
import info.scce.ktml.fake.FakeInlineTag
import info.scce.ktml.fake.util.FakeHref
import org.junit.jupiter.api.Test

class ATest {

    @Test
    fun testSecondConstructor() {
        //then
        assertEquals(
            "<a fakeHref><tag></a>",
            //when
            A(
                //given
                FakeHref(),
                FakeInlineTag()
            )
        )
    }

    @Test
    fun testSecondConstructorEmpty() {
        //then
        assertEquals(
            """<a fakeHref></a>""",
            //when
            A(
                //given
                FakeHref(),
            )
        )
    }

    @Test
    fun testThirdConstructor() {
        //then
        assertEquals(
            """<a fakeHref fakeAttribute></a>""",
            //when
            A(
                //given
                Attributes(FakeAttribute()),
                FakeHref(),
            )
        )
    }

    @Test
    fun testThirdConstructorNamedParameter() {
        //then
        assertEquals(
            """<a fakeHref fakeAttribute></a>""",
            //when
            A(
                //given
                attributes = Attributes(FakeAttribute()),
                FakeHref(),
            )
        )
    }

    @Test
    fun testFourthConstructor() {
        //then
        assertEquals(
            """<a fakeClasses fakeHref fakeAttribute></a>""",
            //when
            A(
                //given
                Attributes(FakeAttribute()),
                FakeClasses(),
                FakeHref(),
            )
        )
    }

    @Test
    fun testFourthConstructorNamedParameters() {
        //then
        assertEquals(
            """<a fakeClasses fakeHref fakeAttribute></a>""",
            //when
            A(
                //given
                attributes = Attributes(FakeAttribute()),
                classes = FakeClasses(),
                href = FakeHref(),
            )
        )
    }

    @Test
    fun testFourthConstructorMixingParameters() {
        //then
        assertEquals(
            """<a fakeClasses fakeHref fakeAttribute></a>""",
            //when
            A(
                //given
                attributes = Attributes(FakeAttribute()),
                FakeClasses(),
                href = FakeHref(),
            )
        )
    }

    @Test
    fun testFifthConstructor() {
        //then
        assertEquals(
            """<a fakeClasses fakeHref><tag></a>""",
            //when
            A(
                //given
                FakeClasses(),
                FakeHref(),
                FakeInlineTag(),
            )
        )
    }

    @Test
    fun testFifthConstructorMixedParameters() {
        //then
        assertEquals(
            """<a fakeClasses fakeHref><tag></a>""",
            //when
            A(
                //given
                classes = FakeClasses(),
                FakeHref(),
                FakeInlineTag(),
            )
        )
    }

    @Test
    fun testSixthConstructorMixedParameters() {
        //then
        assertEquals(
            """<a fakeClasses fakeHref><tag></a>""",
            //when
            A(
                //given
                classes = FakeClasses(),
                FakeHref(),
                listOf(FakeInlineTag())
            )
        )
    }
}