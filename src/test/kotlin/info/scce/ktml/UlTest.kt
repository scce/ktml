package info.scce.ktml

import info.scce.ktml.fake.FakeAttribute
import info.scce.ktml.fake.FakeClasses
import info.scce.ktml.fake.FakeLi
import org.junit.jupiter.api.Test

class UlTest {

    @Test
    fun testSecondConstructor() {
        //then
        assertEquals(
            "<ul><li></ul>",
            //when
            Ul(
                //given
                FakeLi()
            )
        )
    }

    @Test
    fun testSecondConstructorEmpty() {
        //then
        assertEquals(
            """<ul></ul>""",
            //when
            Ul(
                //given
            )
        )
    }

    @Test
    fun testThirdConstructor() {
        //then
        assertEquals(
            """<ul fakeAttribute></ul>""",
            //when
            Ul(
                //given
                Attributes(FakeAttribute())
            )
        )
    }

    @Test
    fun testThirdConstructorNamedParameter() {
        //then
        assertEquals(
            """<ul fakeAttribute></ul>""",
            //when
            Ul(
                //given
                attributes = Attributes(FakeAttribute())
            )
        )
    }

    @Test
    fun testFourthConstructor() {
        //then
        assertEquals(
            """<ul fakeClasses fakeAttribute></ul>""",
            //when
            Ul(
                //given
                Attributes(FakeAttribute()),
                FakeClasses()
            )
        )
    }

    @Test
    fun testFourthConstructorNamedParameters() {
        //then
        assertEquals(
            """<ul fakeClasses fakeAttribute></ul>""",
            //when
            Ul(
                //given
                attributes = Attributes(FakeAttribute()),
                classes = FakeClasses()
            )
        )
    }

    @Test
    fun testFourthConstructorMixingParameters() {
        //then
        assertEquals(
            """<ul fakeClasses fakeAttribute></ul>""",
            //when
            Ul(
                //given
                attributes = Attributes(FakeAttribute()),
                FakeClasses()
            )
        )
    }

    @Test
    fun testFifthConstructor() {
        //then
        assertEquals(
            """<ul fakeClasses><li></ul>""",
            //when
            Ul(
                //given
                FakeClasses(),
                FakeLi()
            )
        )
    }

    @Test
    fun testFifthConstructorMixedParameters() {
        //then
        assertEquals(
            """<ul fakeClasses><li></ul>""",
            //when
            Ul(
                //given
                classes = FakeClasses(),
                FakeLi()
            )
        )
    }

    @Test
    fun testSixthConstructorMixedParameters() {
        //then
        assertEquals(
            """<ul fakeClasses><li></ul>""",
            //when
            Ul(
                //given
                classes = FakeClasses(),
                listOf(FakeLi())
            )
        )
    }
}