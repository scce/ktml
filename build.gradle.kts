plugins {
    kotlin("jvm") version "1.9.0"
    id("org.jetbrains.dokka") version "1.9.10"
    java
    `maven-publish`
}

group = "info.scce"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation(kotlin("test"))
}

tasks.test {
    useJUnitPlatform()
}

kotlin {
    jvmToolchain(8)
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = "info.scce"
            artifactId = "ktml"
            version = "1.0-SNAPSHOT"

            from(components["java"])
        }
    }

    repositories {
        maven {
            name = "GitLabPackages"
            url = uri("https://gitlab.com/api/v4/projects/51066450/packages/maven")
            credentials {
                username = "gitlab-ci-token"
                password = System.getenv("CI_JOB_TOKEN")
            }
        }
    }
}

