# KTML

Kotlin-library for creating type safe HTML documents • [Documentation](https://scce.gitlab.io/ktml/)

**Motivation**.
"To begin building a (HTML) tree you need a (DOM) document
([kotlinx.html](https://github.com/Kotlin/kotlinx.html/wiki/DOM-trees))." 
Unlike kotlinx.html, there is no need for a DOM document to build an HTML tree, when using KTML.
Just create an HTML element like you create an object in Kotlin, e.g, `Body()`.

**Principles**.
The main principle of KTML is the context-free creation of HTML trees and subtrees.
All supported HTML elements can be created everywhere in your applications' flow.
While this enables the elegant construction of complex HTML trees, it also eases testing of
KTML classes in an isolated environment.
The type-safe creation of HTML trees is the second driver for this project.
Leveraging Kotlin's type system, KTML prevents the incorrect nesting of tags, e.g.,
nesting block elements into inline elements (`<p><div></div></p>`).
Relying only on plain object-oriented patterns, inspired by the Book
[Elegant Objects](https://www.yegor256.com/elegant-objects.html), constitutes the third and last principle.

**Implementation**.
The library is written in Kotlin 1.9.0, has no further runtime dependencies and a test coverage of 100%.

## Example

```kotlin
import info.scce.ktml.impl.*

fun main() {
    print(
        Html(
            Head(
                Title(
                    Text("Example")
                )
            ),
            Body(
                Text("Hello World!")
            )
        )
    )
}

//generates:
//<!DOCTYPE html>
//<html lang="en">
//    <head>
//        <title>Example</title>
//    </head>
//    <body>Hello World!</body>
//</html>
```

## Expandability and Reusability

KTML is designed to support expandability and reusability as first-class citizens. 
The following example shows how to create a reusable element which implements Bootstrap's [Jumbotron](https://getbootstrap.com/docs/4.6/components/jumbotron/) component.

![](jumbotron.png)

```kotlin
import info.scce.ktml.interfaces.*
import info.scce.ktml.util.*

class Jumbotron(
    private val header: TextInterface = Text("Hello, world!"),
) : AbstractTagBuilder<DivInterface>(), DivInterface {
    override fun build(): DivInterface {
        return Div(
            classes = Classes("jumbotron"),
            H1(
                classes = Classes("display-4"),
                header,
            ),
            P(
                classes = Classes("lead"),
                Text("This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information."),
            ),
            Hr(
                classes = Classes("my-4")
            ),
            P(
                Text("It uses utility classes for typography and spacing to space content out within the larger container.")
            ),
            A(
                attributes = Attributes(Attribute("role", "button")),
                classes = Classes("btn", "btn-primary", "btn-lg"),
                href = Href("#"),
                Text("Learn more"),
            ),
        )
    }
}
```